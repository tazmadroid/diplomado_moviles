package com.cybertech.hilosdiplomados;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cybertech.hilosdiplomados.webservices.Entity;
import com.cybertech.hilosdiplomados.webservices.JSONParser;
import com.cybertech.hilosdiplomados.webservices.Petition;

import org.json.JSONException;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

	private RecyclerView moviesRecyclerView=null;
	private List<Video> videos=null;
	private ProgressBar moviesProgressBar=null;
	private JSONParser jsonParser=null;

	private Thread progresoThread=null;

	public static MainActivityFragment newInstance(){
		MainActivityFragment mainActivityFragment = new MainActivityFragment();
		return mainActivityFragment;
	}

	public MainActivityFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		moviesProgressBar = (ProgressBar) view.findViewById(R.id.moviesProgressBar);
		moviesRecyclerView = (RecyclerView) view.findViewById(R.id.moviesRecyclerView);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					jsonParser=new JSONParser();
					videos = jsonParser.getJSON("http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json",
							new Petition(Entity.NONE));
					getActivity().runOnUiThread(mostrarListaRunnable);
				}catch (Exception e) {
					Looper.prepare();
					Message msj = mensajeError.obtainMessage();
					msj.obj = e.getMessage();
					mensajeError.sendMessage(msj);
					Looper.loop();
					Looper.myLooper().quit();
				}
			}
		}).start();
	}

	private Runnable mostrarListaRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(),
					LinearLayoutManager.VERTICAL,false);
			moviesRecyclerView.setHasFixedSize(true);
			moviesRecyclerView.setLayoutManager(layoutManager);
			VideoAdapter videoAdapter = new VideoAdapter(videos,getActivity());
			moviesRecyclerView.setAdapter(videoAdapter);
			moviesProgressBar.setVisibility(View.INVISIBLE);
		}
	};

	private Handler mensajeError = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.obj != null) {
				moviesProgressBar.setVisibility(View.INVISIBLE);
				Toast.makeText(getActivity(), (String) msg.obj, Toast.LENGTH_LONG)
						.show();
			}
		};
	};



}
