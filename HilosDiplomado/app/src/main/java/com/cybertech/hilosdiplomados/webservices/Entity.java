package com.cybertech.hilosdiplomados.webservices;

public enum Entity
{
    POST,
    GET,
    FRIENDLY,
    NONE
}
