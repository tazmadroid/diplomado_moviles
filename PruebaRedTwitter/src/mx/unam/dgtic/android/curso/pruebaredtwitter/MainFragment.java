package mx.unam.dgtic.android.curso.pruebaredtwitter;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainFragment extends Fragment{
	
    static String TWITTER_CONSUMER_KEY = "2sNrPTAXaY0fvQsk3cTwDmcNK";
    static String TWITTER_CONSUMER_SECRET = "vNIBGTUTmKcS0jvmizL0eAqLy690ATcsXMp9p152ZwFvR1V1Ty";
 
    
    static String PREFERENCE_NAME = "twitter_oauth";
    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
 
    static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";
 
    static final String URL_TWITTER_AUTH = "auth_url";
    static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
 
    
    Button btnLoginTwitter;
    Button btnUpdateStatus;
    Button btnLogoutTwitter;
    EditText txtUpdate;
    TextView lblUpdate;
    TextView lblUserName;
 
    ProgressDialog pDialog;
 
    protected static Twitter twitter;
    private static RequestToken requestToken;
     
    private static SharedPreferences mSharedPreferences;
     
    private ConnectionDetector cd;
    
    private ActualizarStatus actualizar=null;
     
    private long userID=0;
    private User user=null;
    
    AlertDialogManager alert = new AlertDialogManager();
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	return inflater.inflate(R.layout.fragment_main, container, false);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onCreate(savedInstanceState);
    	
    	cd = new ConnectionDetector(getActivity().getApplicationContext());
    	 
        if (!cd.isConnectingToInternet()) {
            alert.showAlertDialog(getActivity(), "Conexión de Internet",
                    "De favor establecer una conexión a Internet", false);
            return;
        }
         
        if(TWITTER_CONSUMER_KEY.trim().length() == 0 || TWITTER_CONSUMER_SECRET.trim().length() == 0){
            alert.showAlertDialog(getActivity(), "Autorización de Twitter", "Verifica tus llaves de autorización de Twitter", false);
            return;
        }
        
        
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onActivityCreated(savedInstanceState);
    	
        btnLoginTwitter = (Button) getActivity().findViewById(R.id.btnLoginTwitter);
        btnUpdateStatus = (Button) getActivity().findViewById(R.id.btnUpdateStatus);
        btnLogoutTwitter = (Button) getActivity().findViewById(R.id.btnLogoutTwitter);
        txtUpdate = (EditText) getActivity().findViewById(R.id.txtUpdateStatus);
        lblUpdate = (TextView) getActivity().findViewById(R.id.lblUpdate);
        lblUserName = (TextView) getActivity().findViewById(R.id.lblUserName);
 
        mSharedPreferences = getActivity().getApplicationContext().getSharedPreferences(
                "MyPref", 0);
        
        btnLoginTwitter.setOnClickListener(new View.OnClickListener() {
     
            @Override
            public void onClick(View arg0) {
                // Call login twitter function
                loginToTwitter();
            }
        });
        
        btnUpdateStatus.setOnClickListener(new View.OnClickListener() {
        	 
            @Override
            public void onClick(View v) {
                String status = txtUpdate.getText().toString();
 
                if (status.trim().length() > 0) {
                    new updateTwitterStatus().execute(status);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Please enter status message", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        
        btnLogoutTwitter.setOnClickListener(new View.OnClickListener() {
         
            @Override
            public void onClick(View arg0) {
                logoutFromTwitter();
            }
        });
     
        if (!isTwitterLoggedInAlready()) {
        	actualizar = new ActualizarStatus();
            actualizar.execute();
        }else{
        	mostrarInterfaz();
        }
    }
    
    @Override
    public void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
        if (!isTwitterLoggedInAlready()) {
        	   actualizar = new ActualizarStatus();
             actualizar.execute();
            }else{
            	mostrarInterfaz();
        }
    }
    
    @Override
    public void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
        if (!isTwitterLoggedInAlready()) {
        	   actualizar = new ActualizarStatus();
             actualizar.execute();
        }else{
        	mostrarInterfaz();
        }
    }
    
    private void mostrarInterfaz(){
    	btnLoginTwitter.setVisibility(View.GONE);
        lblUpdate.setVisibility(View.VISIBLE);
        txtUpdate.setVisibility(View.VISIBLE);
        btnUpdateStatus.setVisibility(View.VISIBLE);
        btnLogoutTwitter.setVisibility(View.VISIBLE);
    }
    
    private void loginToTwitter() {
    	new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (!isTwitterLoggedInAlready()) {
		            ConfigurationBuilder builder = new ConfigurationBuilder();
		            builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
		            builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
		            Configuration configuration = builder.build();
		             
		            TwitterFactory factory = new TwitterFactory(configuration);
		            twitter = factory.getInstance();
		 
		            try {
		                requestToken = twitter
		                        .getOAuthRequestToken(TWITTER_CALLBACK_URL);
		                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri
		                        .parse(requestToken.getAuthenticationURL())));
		            } catch (TwitterException e) {
		                e.printStackTrace();
		            }
			}else{
				  mostrarInterfaz();
			}
			}
		}).start();
    }
 
    private boolean isTwitterLoggedInAlready() {
        return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
    }
    
    private void logoutFromTwitter() {
        Editor e = mSharedPreferences.edit();
        e.remove(PREF_KEY_OAUTH_TOKEN);
        e.remove(PREF_KEY_OAUTH_SECRET);
        e.remove(PREF_KEY_TWITTER_LOGIN);
        e.commit();
     
        btnLogoutTwitter.setVisibility(View.GONE);
        btnUpdateStatus.setVisibility(View.GONE);
        txtUpdate.setVisibility(View.GONE);
        lblUpdate.setVisibility(View.GONE);
        lblUserName.setText("");
        lblUserName.setVisibility(View.GONE);
     
        btnLoginTwitter.setVisibility(View.VISIBLE);
    }
    
    class updateTwitterStatus extends AsyncTask<String, String, String> {

    	   @Override
    	   protected void onPreExecute() {
    	       super.onPreExecute();
    	       pDialog = new ProgressDialog(getActivity());
    	       pDialog.setMessage("Updating to twitter...");
    	       pDialog.setIndeterminate(false);
    	       pDialog.setCancelable(false);
    	       pDialog.show();
    	   }

    	   protected String doInBackground(String... args) {
    	       String status = args[0];
    	       try {
    	           ConfigurationBuilder builder = new ConfigurationBuilder();
    	           builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
    	           builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
    	            
    	           String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");

    	           String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");
    	            
    	           AccessToken accessToken = new AccessToken(access_token, access_token_secret);
    	           Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);
    	            
    	           twitter4j.Status response = twitter.updateStatus(status);
    	            
    	       } catch (TwitterException e) {
    	           Log.d("Error Twitter Update", e.getMessage());
    	       }
    	       return null;
    	   }

    	   protected void onPostExecute(String file_url) {
    	       pDialog.dismiss();
    	       getActivity().runOnUiThread(new Runnable() {
    	           @Override
    	           public void run() {
    	               Toast.makeText(getActivity().getApplicationContext(),
    	                       "Tweet ha sido publicado", Toast.LENGTH_SHORT)
    	                       .show();
    	               txtUpdate.setText("");
    	           }
    	       });
    	   }

    	}
    
    class ActualizarStatus extends AsyncTask<Void, Integer, Boolean>{

    	AccessToken accessToken = null;
    	
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Uri uri = getActivity().getIntent().getData();
            if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
                String verifier = uri
                        .getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
     
                try {
                    accessToken = twitter.getOAuthAccessToken(
                            requestToken, verifier);
     
                    Editor e = mSharedPreferences.edit();
     
                    e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
                    e.putString(PREF_KEY_OAUTH_SECRET,
                            accessToken.getTokenSecret());
                    e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
                    e.commit();
     
                    return true;
                } catch (Exception e){
                	Log.e("Error Twitter Login", "> " + e.getMessage());
                	return false;
                }
            }else{
            	return false;
            }
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result){
				try{
					btnLoginTwitter.setVisibility(View.GONE);
		            lblUpdate.setVisibility(View.VISIBLE);
		            txtUpdate.setVisibility(View.VISIBLE);
		            btnUpdateStatus.setVisibility(View.VISIBLE);
		            btnLogoutTwitter.setVisibility(View.VISIBLE);
		             
		            userID = accessToken.getUserId();
		            user = twitter.showUser(userID);
		            String username = user.getName();
		             
		            lblUserName.setText(Html.fromHtml("<b>Welcome " + username + "</b>"));
				}catch(Exception e){
					Log.v("Error", e.getMessage());
				}
				
			}
			
		}
    	
    }

}

