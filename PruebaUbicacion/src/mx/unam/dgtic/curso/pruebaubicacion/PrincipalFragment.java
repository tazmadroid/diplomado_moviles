package mx.unam.dgtic.curso.pruebaubicacion;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class PrincipalFragment extends Fragment{
	
	private GoogleMap mapa=null;
	private Marker ubicacion=null;
	private Button ubicacionButton=null;
	private TextView latitudTextView=null;
	private TextView longitudTextView=null;
	private LocationManager locMan=null;
	private GPS gps=null;
	private LatLng geoPuntoUbicacion=null;
	
	private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
	
	public static PrincipalFragment newInstance(){
		PrincipalFragment principalFragment = new PrincipalFragment();
		return principalFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_main, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if(checkPlayServices()){
			mapa = ((SupportMapFragment) getActivity().getSupportFragmentManager()
			          .findFragmentById(R.id.mapa)).getMap();
			mapa.setMyLocationEnabled(true);
		    mapa.setTrafficEnabled(false);
		}
		ubicacionButton = (Button) getActivity().findViewById(R.id.ubicacionButton);
		latitudTextView = (TextView) getActivity().findViewById(R.id.latitudTextView);
		longitudTextView = (TextView) getActivity().findViewById(R.id.longitudTextView);
		
		
	    locMan = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		ubicacionButton.setOnClickListener(ubicacionListener);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
	      case REQUEST_CODE_RECOVER_PLAY_SERVICES:
	        if (resultCode == getActivity().RESULT_CANCELED) {
	          Toast.makeText(getActivity().getApplicationContext(),
	              "Google Play Services no ha sido instalado", Toast.LENGTH_LONG)
	              .show();
	          getActivity().finish();
	        }
	        return;
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	private OnClickListener ubicacionListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(Internet.verificarConexion(getActivity())){
				if(!(GPS.verificarGPS(locMan))){
					Toast.makeText(getActivity(), "No se encuentra activado el GPS", Toast.LENGTH_LONG).show();
					gps = new GPS(getActivity().getBaseContext(),locMan);
				}else{
					gps = new GPS(getActivity().getBaseContext(),locMan);
				}
				
				geoPuntoUbicacion = gps.getObtenerUbicacionActual();
				if(geoPuntoUbicacion!=null){
					mostrarUbicacion(geoPuntoUbicacion);
				}else{
					if(checkPlayServices()){
					Location punto = mapa.getMyLocation();
			          if (punto != null) {
			            geoPuntoUbicacion = new LatLng(punto.getLatitude(),
			                punto.getLongitude());
			            if (geoPuntoUbicacion == null) {
			              Toast.makeText(getActivity().getBaseContext(), "No se encuentra la ubicación actual",
			                  Toast.LENGTH_LONG).show();
			            } else {
			              mostrarUbicacion(geoPuntoUbicacion);
			            }
			          } else {
			            Toast.makeText(getActivity().getBaseContext(), "No es posible obtener la ubicación", Toast.LENGTH_LONG).show();
			          }
					}
				}
			}else{
				Toast.makeText(getActivity(), "No se encuentra conectado a Internet", Toast.LENGTH_LONG).show();
			}
		}
	};
	
	private void mostrarUbicacion(LatLng ubicacionGPS){
		if(checkPlayServices()){
		ubicacion=mapa.addMarker(new MarkerOptions()
        .position(ubicacionGPS).title("Ubicación Actual").draggable(false)
        .flat(true)
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
		
		centrarMapa(ubicacionGPS, 9);
		}
		
		
		latitudTextView.setText(" "+ubicacionGPS.latitude);
		longitudTextView.setText(" "+ubicacionGPS.longitude);
	}
	
	private void centrarMapa(LatLng punto, float zoom) {
	    CameraPosition posicion = new CameraPosition.Builder().target(punto)
	        .zoom(zoom).bearing(0).tilt(0).build();
	    CameraUpdate update = CameraUpdateFactory.newCameraPosition(posicion);
	    mapa.animateCamera(update);
	  }
	
	private boolean checkPlayServices() {
	    int status = GooglePlayServicesUtil
	        .isGooglePlayServicesAvailable(getActivity().getApplicationContext());
	    if (status != ConnectionResult.SUCCESS) {
	      if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
	        mostrarErrorPlayServices(status);
	      } else {
	        Toast.makeText(getActivity().getApplicationContext(),
	            "Este dispositivo no soporta los servicios de Google Play",
	            Toast.LENGTH_LONG).show();
	        getActivity().finish();
	      }
	      return false;
	    } else {
	      return true;
	    }
	  }
	  
	  private void mostrarErrorPlayServices(int status) {
	    GooglePlayServicesUtil.getErrorDialog(status, getActivity(), REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
	  }

}
