package mx.unam.dgtic.curso.pruebaubicacion;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Internet {
  
  protected static boolean verificarConexion(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    
    if (cm != null) {
      NetworkInfo netinfo[] = cm.getAllNetworkInfo();
      
      if (netinfo != null) {
        for (NetworkInfo info : netinfo) {
          if (info.getState() == NetworkInfo.State.CONNECTED) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
}