package mx.unam.dgtic.curso.pruebaubicacion;

import java.util.List;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class GPS {
  
  private LocationManager locManager;
  private double latitud;
  private double longitud;
  private GPSHelper gpsListener;
  private Context ctx;
  private Location loc;
  private String provedor;
  public static final int radioBusqueda = 400;
  
  public GPS() {
    
  }
  
  public GPS(Context ctx, LocationManager locManager) {
    this.ctx = ctx;
    this.locManager = locManager;
    obtenerPosicion();
  }
  
  public double getLatitud() {
    return (latitud);
  }
  
  public double getLongitud() {
    return (longitud);
  }
  
  public static boolean verificarGPS(LocationManager locMan) {
    boolean active = false;
    if (locMan.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
      active = true;
    }
    return active;
  }
  
  public boolean verificarCoordenadas() {
    boolean activo = false;
    if (this.latitud != 0 && this.longitud != 0)
      activo = true;
    return activo;
  }
  
  private void setBestProvedor() {
    Criteria req = new Criteria();
    req.setAccuracy(Criteria.ACCURACY_FINE);
    req.setAltitudeRequired(true);
    req.setPowerRequirement(Criteria.POWER_MEDIUM);
    provedor = locManager.getBestProvider(req, false);
    if (!(locManager.isProviderEnabled(provedor))) {
      setProvedor();
    }
  }
  
  private void setProvedor() {
    boolean active = false;
    List<String> provedores = locManager.getAllProviders();
    int cont = provedores.size();
    for (int i = 0; i < cont; i++) {
      if (locManager.isProviderEnabled(provedores.get(i))
          && provedores.get(i).equals(LocationManager.NETWORK_PROVIDER)) {
        provedor = provedores.get(i);
        active = true;
      } else if (!active) {
        provedor = null;
      }
    }
  }
  
  public void obtenerPosicion() {
    try {
      detenerLocalizacion();
      gpsListener = new GPSHelper();
      setBestProvedor();
      if (provedor != null) {
        locManager.requestLocationUpdates(provedor, 0, 0, gpsListener);
        if (this.loc == null) {
          setProvedor();
          this.loc = locManager.getLastKnownLocation(provedor);
          if (this.loc != null) {
            obtenerPosicion();
          } else {
            detenerLocalizacion();
            Toast msj = Toast.makeText(ctx,
                "No es posible obtener la ubicación actual", Toast.LENGTH_LONG);
            msj.show();
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void setLocation(Location loc) {
    this.loc = loc;
  }
  
  public LatLng getObtenerUbicacionActual() {
    LatLng punto = null;
    try {
      if (verificarCoordenadas()) {
        this.latitud = this.loc.getLatitude();
        this.longitud = this.loc.getLongitude();
        punto = new LatLng(this.latitud, this.longitud);
      } else {
        punto = null;
      }
      
    } catch (Exception e) {
      punto = null;
    }
    return (punto);
  }
  
  public String getProvedor() {
    return (provedor);
  }
  
  public void detenerLocalizacion() {
    if (gpsListener != null) {
      locManager.removeUpdates(gpsListener);
    }
  }
  
  public float calcularDistancia(double latitudParada, double longitudParada) {
    float distancia = 0;
    float[] results = new float[3];
    Location.distanceBetween(latitud, longitud, latitudParada, longitudParada,
        results);
    distancia = results[0];
    
    return distancia;
  }
  
  class GPSHelper implements LocationListener {
    
    @Override
    public void onLocationChanged(Location location) {
      // TODO Auto-generated method stub
      
      if (location != null) {
        setLocation(location);
      } else {
        Toast.makeText(ctx, "No se encuentra la ubicación", Toast.LENGTH_LONG)
            .show();
        setProvedor();
        // obtenerPosicion();
      }
      detenerLocalizacion();
    }
    
    @Override
    public void onProviderDisabled(String provider) {
      // TODO Auto-generated method stub
      
    }
    
    @Override
    public void onProviderEnabled(String provider) {
      // TODO Auto-generated method stub
      
    }
    
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
      // TODO Auto-generated method stub
      
    }
    
  }
}
