package com.cybertech.videoplayerdiplomado;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;


public class PlayerFragment extends Fragment implements SurfaceHolder.Callback,
		MediaPlayer.OnPreparedListener {

	private MediaPlayer mediaPlayer=null;
	private SurfaceHolder videoHolder=null;
	private SurfaceView videoSurface=null;
	String videoUrl="https://archive.org/download/" +
			"ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_player, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		videoSurface = (SurfaceView) getActivity().findViewById(R.id.videoSurfaceView);
		videoHolder = videoSurface.getHolder();
		videoHolder.addCallback(this);
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		mediaPlayer.start();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		try{
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setDisplay(videoHolder);
			mediaPlayer.setDataSource(videoUrl);
			mediaPlayer.prepare();
			mediaPlayer.setOnPreparedListener(this);
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		}catch(Exception e){

		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
														 int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub

	}
}
