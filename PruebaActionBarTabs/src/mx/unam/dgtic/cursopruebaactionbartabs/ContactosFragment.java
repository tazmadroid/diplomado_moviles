package mx.unam.dgtic.cursopruebaactionbartabs;

import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ContactosFragment extends Fragment{
	
	private ListView contactosListView=null;
	private AdapterContacto contactoAdapter=null;
	
	private ContactosListener contactosListener=null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.contactos_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		contactosListView = (ListView) getActivity().findViewById(R.id.contactosListView);
		mostrarListaContactos(llenarContactos());
	}
	
	
	public void mostrarListaContactos(List<Contacto> contactos){
		contactoAdapter = new AdapterContacto(getActivity().getBaseContext(), contactos);
		contactosListView.setAdapter(contactoAdapter);
		contactosListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				if(contactosListener!=null){
					Contacto contacto =(Contacto)contactosListView.getAdapter().getItem(position);
					contactosListener.mostrarContactoSeleccionado(contacto );
				}
			}
		});
	}
	
	public interface ContactosListener{
		void mostrarContactoSeleccionado(Contacto contacto);
	}
	
	public void setContactosListener(ContactosListener contactosListener){
		this.contactosListener=contactosListener;
	}
	
	public List<Contacto> llenarContactos(){
		List<Contacto> contactos = new ArrayList<Contacto>();
		for(int indice=0;indice<20;indice++){
			contactos.add(new Contacto(indice,"Juan"+indice,"Flores",
					"juan@hotmail.com","centro"));
		}
		return (contactos);
	}
}
