package mx.unam.dgtic.android.pruebaactionbarcompat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private ShareActionProvider compartir=null;
	private SearchView searchView=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem shareItem = menu.findItem(R.id.action_share);
	    compartir = (ShareActionProvider) MenuItemCompat
	        .getActionProvider(shareItem);
	    compartir.setShareIntent(getDefaultIntent());
	    MenuItem searchItem = menu.findItem(R.id.action_search);
	    searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
	    searchView.setOnQueryTextListener(searchViewListener);
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_search:
			searchView.setIconified(false);
			return true;
		case R.id.actionAcerca:
			Toast.makeText(getApplicationContext(), "Acerca oprimido", Toast.LENGTH_LONG).show();
			return true;
		case R.id.actionSave:
			Toast.makeText(getApplicationContext(), "Guardar oprimido", Toast.LENGTH_LONG).show();
			return true;
		case R.id.actionSettings:
			Toast.makeText(getApplicationContext(), "Configuraci�n oprimido", Toast.LENGTH_LONG).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
			
		}
		
	}
	
	private OnQueryTextListener searchViewListener = new OnQueryTextListener() {
		
		@Override
		public boolean onQueryTextSubmit(String arg0) {
			// TODO Auto-generated method stub
			Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
			searchIntent.putExtra("keySearch", arg0);
			startActivity(searchIntent);
			return true;
		}
		
		@Override
		public boolean onQueryTextChange(String arg0) {
			// TODO Auto-generated method stub
			
			return true;
		}
	};
	
	private Intent getDefaultIntent() {
	    StringBuilder urlActividad= new StringBuilder("www.google.com.mx");
	    Intent intent = new Intent(Intent.ACTION_SEND);
	    intent.putExtra(Intent.EXTRA_TEXT, urlActividad.toString());
	    intent.setType("text/plain");
	    return intent;
	}
}
