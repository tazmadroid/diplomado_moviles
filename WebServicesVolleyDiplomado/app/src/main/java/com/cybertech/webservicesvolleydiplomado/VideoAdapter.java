package com.cybertech.webservicesvolleydiplomado;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import java.util.List;

/**
 * Created by jaivetorrespineda on 15/02/18.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

	private List<Video> videos=null;
	private Context context=null;

	public VideoAdapter(List<Video> videos, Context context) {
		this.videos = videos;
		this.context = context;
	}

	public VideoAdapter(List<Video> videos){
		this.videos=videos;
	}

	@Override
	public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.
				item_video, parent,false);
		return new VideoViewHolder(item);
	}

	@Override
	public void onBindViewHolder(VideoViewHolder holder, int position) {
		holder.titleTextView.setText(videos.get(position).getTitle());
		holder.typeTextView.setText(videos.get(position).getType());
		Glide
				.with(holder.thumbnailImageView.getContext())
				.load(videos.get(position).getPoster())
				.into(holder.thumbnailImageView);
	}

	@Override
	public int getItemCount() {
		return videos.size();
	}


	static class VideoViewHolder extends RecyclerView.ViewHolder{

		protected TextView titleTextView=null;
		protected TextView typeTextView=null;
		protected ImageView thumbnailImageView=null;

		public VideoViewHolder(View itemView) {
			super(itemView);
			titleTextView=(TextView) itemView.findViewById(R.id.titleTextView);
			typeTextView=(TextView) itemView.findViewById(R.id.typeTextView);
			thumbnailImageView=(ImageView) itemView.findViewById(R.id.thumbnailImageView);
		}
	}
}

