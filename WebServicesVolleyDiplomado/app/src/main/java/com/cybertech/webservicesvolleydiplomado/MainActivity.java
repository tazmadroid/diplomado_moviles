package com.cybertech.webservicesvolleydiplomado;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	private VolleyService volleyService=null;
	private RequestQueue requestQueue=null;

	private RecyclerView videosRecyclerView=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		volleyService=VolleyService.getInstance(getApplicationContext());

		videosRecyclerView= (RecyclerView) findViewById(R.id.videosRecyclerView);
		LinearLayoutManager layoutManager= new LinearLayoutManager(this,
				LinearLayoutManager.VERTICAL,false);
		videosRecyclerView.setHasFixedSize(true);
		videosRecyclerView.setLayoutManager(layoutManager);

		getVideos();

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});
	}

	public void getVideos(){
		String url="http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json";
		JsonObjectRequest request =new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				VideoAdapter videoAdapter = new VideoAdapter(getJSON(response),getBaseContext());
				videosRecyclerView.setAdapter(videoAdapter);

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(MainActivity.this, "No se obtuvo la información", Toast.LENGTH_SHORT).show();
			}
		});
		volleyService.addRequest(request);
	}

	public List<Video> getJSON(JSONObject videosJsonObject){
		List<Video> videos =new ArrayList<>();
		try {
			if(videosJsonObject!=null){
				JSONArray videosJsonArray = videosJsonObject.getJSONArray("Search");
				if(videosJsonArray!=null){
					for (int indice=0;indice<videosJsonArray.length();indice++){
						JSONObject videoJSoJsonObject= videosJsonArray.getJSONObject(indice);
						videos.add(new Video(videoJSoJsonObject.getString("imdbID"),
								videoJSoJsonObject.getString("Title"),
								videoJSoJsonObject.getString("Year"),
								videoJSoJsonObject.getString("Type"),
								videoJSoJsonObject.getString("Poster")));
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return videos;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
