package mx.unam.dgtic.curso.pruebacamara;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends ActionBarActivity {

	private ImageButton camaraImageButton = null;
	private ImageButton videoImageButton = null;
	private ImageView fotoImageView = null;
	private VideoView grabacionVideoView = null;

	private Uri archivoUri = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		camaraImageButton = (ImageButton) findViewById(R.id.camaraImageButton);
		videoImageButton = (ImageButton) findViewById(R.id.videoImageButton);
		fotoImageView = (ImageView) findViewById(R.id.fotoImageView);
		grabacionVideoView = (VideoView) findViewById(R.id.grabacionVideoView);

		camaraImageButton.setOnClickListener(camaraOnClickListener);
		videoImageButton.setOnClickListener(videoOnClickListener);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				imagenPrevia();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} else if (requestCode == Utils.CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// video successfully recorded
				// preview the recorded video
				videoPrevia();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(getApplicationContext(),
						"User cancelled video recording", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to record video", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable("archivo_uri", archivoUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		archivoUri = savedInstanceState.getParcelable("archivo_uri");
	}

	private OnClickListener camaraOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			capturarImagen();
		}
	};

	private OnClickListener videoOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			capturarVideo();
		}
	};

	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			return true;
		} else {
			return false;
		}
	}

	private void capturarImagen() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		archivoUri = leerArchivoMediaUri(Utils.MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, archivoUri);

		startActivityForResult(intent, Utils.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	private void capturarVideo() {
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

		archivoUri = leerArchivoMediaUri(Utils.MEDIA_TYPE_VIDEO);

		// set video quality
		// 1- for high quality video
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, archivoUri);

		// start the video capture Intent
		startActivityForResult(intent, Utils.CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
	}

	private void imagenPrevia() {
		try {

			grabacionVideoView.setVisibility(View.GONE);

			fotoImageView.setVisibility(View.VISIBLE);

			BitmapFactory.Options options = new BitmapFactory.Options();

			options.inSampleSize = 8;

			final Bitmap bitmap = BitmapFactory.decodeFile(
					archivoUri.getPath(), options);

			fotoImageView.setImageBitmap(bitmap);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void videoPrevia() {
		try {
			// hide image preview
			fotoImageView.setVisibility(View.GONE);

			grabacionVideoView.setVisibility(View.VISIBLE);
			grabacionVideoView.setVideoPath(archivoUri.getPath());
			// start playing
			grabacionVideoView.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Uri leerArchivoMediaUri(int type) {
		return Uri.fromFile(leerArchivoMedia(type));
	}

	private static File leerArchivoMedia(int type) {

		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				Utils.IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(Utils.IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ Utils.IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == Utils.MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else if (type == Utils.MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
}
