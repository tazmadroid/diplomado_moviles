package mx.unam.dgtic.curso.pruebacamara;

public class Utils {

    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    
    public static final String IMAGE_DIRECTORY_NAME = "Prueba Camara";
}
