package mx.unam.dgtic.curso.pruebaserviciojson;

import java.util.List;

import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListaContactosFragment extends Fragment{

	private ListView contactosListView=null;
	private JSONParser parser=null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_main, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		try{
			parser = new JSONParser("http://diplomado.cybertechdroid.com/Webservice/");
			contactosListView = (ListView) getActivity().findViewById(R.id.contactosListView);
			List<Contacto> contactos=parser.obtenerContactos();
			if(contactos!=null && contactos.size()!=0){
				ArrayAdapter<Contacto> contactosAdapter = new ArrayAdapter<Contacto>(getActivity(),android.R.layout.simple_spinner_item, contactos);
				contactosListView.setAdapter(contactosAdapter);
			}else{
				Toast.makeText(getActivity(), "No hay contactos", Toast.LENGTH_SHORT).show();
			}
			
		}catch(JSONException e){
			Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}catch (Exception f) {
			// TODO: handle exception
			Toast.makeText(getActivity(), f.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
	}
}
