package com.cybertech.servicioswebdiplomado;

/**
 * Created by Tazmadroid on 24/02/18.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cybertech.servicioswebdiplomado.webservices.Entity;
import com.cybertech.servicioswebdiplomado.webservices.JSONParser;
import com.cybertech.servicioswebdiplomado.webservices.Petition;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Created by jaivetorrespineda on 15/02/18.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

  private List<Video> videos=null;

  public VideoAdapter(List<Video> videos){
    this.videos=videos;
  }

  @Override
  public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.
        item_video, parent,false);
    return new VideoViewHolder(item);
  }

  @Override
  public void onBindViewHolder(VideoViewHolder holder, int position) {
    holder.titleTextView.setText(videos.get(position).getTitle());
    holder.typeTextView.setText(videos.get(position).getType());
    BitmapFactory.Options bmOptions;
    bmOptions = new BitmapFactory.Options();
    bmOptions.inSampleSize = 1;
    Bitmap bm = LoadImage(videos.get(position).getPoster(), bmOptions);
    holder.thumbnailImageView.setImageBitmap(bm);
  }

  @Override
  public int getItemCount() {
    return videos.size();
  }

  private Bitmap LoadImage(String URL, BitmapFactory.Options options)
  {
    Bitmap bitmap = null;
    InputStream in = null;
    try {
      JSONParser rss = new JSONParser();
      in = rss.createConnection(URL,new Petition(Entity.NONE)).getInputStream();
      bitmap = BitmapFactory.decodeStream(in, null, options);
      in.close();
    } catch (IOException e1) {
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bitmap;
  }

  static class VideoViewHolder extends RecyclerView.ViewHolder{

    protected TextView titleTextView=null;
    protected TextView typeTextView=null;
    protected ImageView thumbnailImageView=null;

    public VideoViewHolder(View itemView) {
      super(itemView);
      titleTextView=(TextView) itemView.findViewById(R.id.titleTextView);
      typeTextView=(TextView) itemView.findViewById(R.id.typeTextView);
      thumbnailImageView=(ImageView) itemView.findViewById(R.id.thumbnailImageView);
    }
  }
}
