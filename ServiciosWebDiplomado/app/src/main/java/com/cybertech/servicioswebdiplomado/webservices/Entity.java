package com.cybertech.servicioswebdiplomado.webservices;

/**
 * Created by Tazmadroid on 24/02/18.
 */

public enum Entity {
  POST,
  GET,
  FRIENDLY,
  NONE
}
