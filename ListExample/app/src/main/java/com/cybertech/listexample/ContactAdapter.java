package com.cybertech.listexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {

  private Context context=null;
  private List<Contact> contacts=null;

  public ContactAdapter(@NonNull Context context, @NonNull List<Contact> objects) {
    super(context, R.layout.item_contact, objects);
    this.context=context;
    this.contacts=objects;
  }

  @Override
  public int getCount() {
    return contacts.size();
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    View item=convertView;
    ContactViewHolder contactViewHolder;
    if(item==null){
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.
          LAYOUT_INFLATER_SERVICE);
      item=inflater.inflate(R.layout.item_contact,null);
      contactViewHolder = new ContactViewHolder();
      contactViewHolder.imageView=item.findViewById(R.id.imageView);
      contactViewHolder.nameTextView=item.findViewById(R.id.name_textview);
      contactViewHolder.lastnameTextView=item.findViewById(R.id.lastname_textview);
      item.setTag(contactViewHolder);
    }else{
      contactViewHolder =(ContactViewHolder) item.getTag();
    }

    contactViewHolder.imageView.setImageResource(R.mipmap.ic_launcher_round);
    contactViewHolder.nameTextView.setText(contacts.get(position).getName());
    contactViewHolder.lastnameTextView.setText(contacts.get(position).getLastname());

    return item;
  }

  static class ContactViewHolder{
    ImageView imageView;
    TextView nameTextView;
    TextView lastnameTextView;
  }
}
