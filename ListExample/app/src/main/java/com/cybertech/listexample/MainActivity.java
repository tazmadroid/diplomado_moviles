package com.cybertech.listexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private ListView contactsListView=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    contactsListView=findViewById(R.id.contacts_listview);

    ContactAdapter adapter = new ContactAdapter(getBaseContext(),getContacts());
    contactsListView.setAdapter(adapter);
    contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Contact contactSelected= ((ContactAdapter)adapterView.getAdapter()).getItem(i);
        Toast.makeText(getBaseContext(),contactSelected.toString(),Toast.LENGTH_LONG).show();
      }
    });
  }

  private List<Contact> getContacts(){
    List<Contact> contacts = new ArrayList<>();
    for (int indice=0;indice<=20;indice++){
      contacts.add(new Contact(indice,"Alumno"+indice,
          "Apellido"+indice,"Email " +
          indice,indice));
    }
    return contacts;
  }
}
