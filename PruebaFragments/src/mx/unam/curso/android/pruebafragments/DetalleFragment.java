package mx.unam.curso.android.pruebafragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class DetalleFragment extends Fragment{
	
	private TextView nombreTextView=null;
	private TextView apellidosTextView=null;
	private TextView direccionTextView=null;
	private TextView correoTextView=null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.detalle_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	
		nombreTextView = (TextView) getActivity().findViewById(R.id.nombreTextView);
		apellidosTextView = (TextView) getActivity().findViewById(R.id.apellidosTextView);
		direccionTextView = (TextView) getActivity().findViewById(R.id.direccionTextView);
		correoTextView = (TextView) getActivity().findViewById(R.id.correoTextView);
		
	}
	
	public void mostrarDetalle(Contacto contacto){
		nombreTextView = (TextView) getActivity().findViewById(R.id.nombreTextView);
		apellidosTextView = (TextView) getActivity().findViewById(R.id.apellidosTextView);
		direccionTextView = (TextView) getActivity().findViewById(R.id.direccionTextView);
		correoTextView = (TextView) getActivity().findViewById(R.id.correoTextView);
		
		nombreTextView.setText(contacto.getNombre());
		apellidosTextView.setText(contacto.getApellidos());
		direccionTextView.setText(contacto.getDireccion());
		correoTextView.setText(contacto.getCorreo());
	}
}
