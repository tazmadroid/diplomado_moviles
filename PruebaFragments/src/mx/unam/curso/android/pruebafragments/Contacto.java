package mx.unam.curso.android.pruebafragments;

import java.io.Serializable;

import android.graphics.drawable.Drawable;

public class Contacto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6223890962116358979L;
	private int id=0;
	private String nombre=null;
	private String apellidos=null;
	private String correo=null;
	private String direccion=null;
	//private Drawable contacto=null;
	
	public Contacto(){
		
	}
	
	public Contacto(int id, String nombre, String apellidos, String correo,
			String direccion){
		this.id=id;
		this.nombre=nombre;
		this.apellidos=apellidos;
		this.correo=correo;
		this.direccion=direccion;
		//this.contacto=contacto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/*public Drawable getContacto() {
		return contacto;
	}

	public void setContacto(Drawable contacto) {
		this.contacto = contacto;
	}*/
	
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if(o instanceof Contacto){
			Contacto cont=(Contacto) o;
			if(this.id==cont.id && this.nombre.equals(cont.nombre)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.id*5+this.nombre.hashCode();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.nombre + " " + this.apellidos;
	}

}
