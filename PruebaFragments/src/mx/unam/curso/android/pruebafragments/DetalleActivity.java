package mx.unam.curso.android.pruebafragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class DetalleActivity extends FragmentActivity{

	private DetalleFragment detalleFragment=null;
	private Contacto contacto=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle);
		
		Bundle detalleBundle=getIntent().getExtras();
		contacto=(Contacto)detalleBundle.getSerializable("contacto");
		
		detalleFragment=(DetalleFragment) getSupportFragmentManager().findFragmentById(R.id.detalleFragment);
		detalleFragment.mostrarDetalle(contacto);
	}
}
