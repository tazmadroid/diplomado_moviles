package mx.unam.curso.android.pruebafragments;

import mx.unam.curso.android.pruebafragments.ContactosFragment.ContactosListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

public class MainActivity extends FragmentActivity {

	ContactosFragment contactosFragment=null;
	DetalleFragment detalleFragment=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		contactosFragment =(ContactosFragment) getSupportFragmentManager().findFragmentById(R.id.contactosFragment);
		contactosFragment.setContactosListener(contactosListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private ContactosListener contactosListener = new ContactosListener() {
		
		@Override
		public void mostrarContactoSeleccionado(Contacto contacto) {
			// TODO Auto-generated method stub
			boolean existeDetalle=(getSupportFragmentManager().findFragmentById(R.id.detalleFragment)!=null);
			
			if(existeDetalle){
				detalleFragment=(DetalleFragment) getSupportFragmentManager().findFragmentById(R.id.detalleFragment);
				detalleFragment.mostrarDetalle(contacto);
			}else{
				Intent detalleIntent=new Intent(MainActivity.this,DetalleActivity.class);
				detalleIntent.putExtra("contacto", contacto);
				startActivity(detalleIntent);
			}
		}
	};
	
}
