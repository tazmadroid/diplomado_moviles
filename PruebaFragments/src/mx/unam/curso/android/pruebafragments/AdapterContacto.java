package mx.unam.curso.android.pruebafragments;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterContacto extends ArrayAdapter<Contacto>{

	private Context context=null;
	private List<Contacto> contactos=null;
	
	public AdapterContacto(Context context, List<Contacto> contactos) {
		super(context, R.layout.item_contacto, contactos);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.contactos=contactos;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View item = inflater.inflate(R.layout.item_contacto, parent, false);
		
		TextView nombreTextView = (TextView) item.findViewById(R.id.nombreTextView);
		
		TextView apellidoTextView = (TextView) item.findViewById(R.id.apellidoTextView);
		
		nombreTextView.setText(contactos.get(position).getNombre());
		
		apellidoTextView.setText(contactos.get(position).getApellidos());
		    
		return item;
	}
	
	@Override
	public Contacto getItem(int position) {
		// TODO Auto-generated method stub
		return this.contactos.get(position);
	}

}
