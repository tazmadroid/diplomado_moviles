package com.eluniversal.test.audiodiplomado;

import android.app.Dialog;

/**
 * ElUniversal
 * com.msi.eluniversal.listeners
 * <p/>
 * Creado por:I.S.C. Jaive Torres Pineda .
 * Descripción:
 * Fecha de creación: 03/03/16.
 * Última actualización:03/03/16.
 * Actualizado por:
 */
public interface OnDialogWarningListener {

  public void onAccept (Dialog dialog);

  public void onCancel (Dialog dialog);
}
