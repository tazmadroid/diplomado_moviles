package com.eluniversal.test.audiodiplomado;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;
import java.util.Random;

public class MusicaService extends Service implements
		MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
		MediaPlayer.OnCompletionListener {

	private MediaPlayer reproductor = null;
	private List<Cancion> canciones = null;
	private int posicionCancion = 0;
	private final IBinder musicaBind = new MusicaBinder();
	private String tituloCancion = "";
	private static final int NOTIFY_ID = 1;
	// shuffle flag and random
	private boolean shuffle = false;
	private Random random;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		posicionCancion = 0;
		// random
		random = new Random();
		// create player
		reproductor = new MediaPlayer();
		// initialize
		inicializarReproductor();

	}

	public void inicializarReproductor() {
		reproductor.setWakeMode(getApplicationContext(),
				PowerManager.PARTIAL_WAKE_LOCK);
		reproductor.setAudioStreamType(AudioManager.STREAM_MUSIC);
		// set listeners
		reproductor.setOnPreparedListener(this);
		reproductor.setOnCompletionListener(this);
		reproductor.setOnErrorListener(this);
	}

	public void setCanciones(List<Cancion> canciones) {
		this.canciones = canciones;
	}

	public void reproducirCancion() {
		// play
		reproductor.reset();
		// get song
		Cancion cancion = canciones.get(posicionCancion);
		// get title
		tituloCancion = cancion.getTitulo();
		// get id
		long cancionActual = cancion.getId();
		// set uri
		Uri trackUri = ContentUris.withAppendedId(
				android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
				cancionActual);
		// set the data source
		try {
			reproductor.setDataSource(getApplicationContext(), trackUri);
		} catch (Exception e) {
			Log.e("MUSIC SERVICE", "Error setting data source", e);
		}
		reproductor.prepareAsync();

	}
	
	//toggle shuffle
		public void setShuffle(){
			if(shuffle) shuffle=false;
			else shuffle=true;
		}

	public void setCancion(int indice) {
		this.posicionCancion = indice;
	}

	// playback methods
	public int getPosicion() {
		return reproductor.getCurrentPosition();
	}

	public int getDur() {
		return reproductor.getDuration();
	}

	public boolean isPng() {
		return reproductor.isPlaying();
	}

	public void pauseReproductor() {
		reproductor.pause();
	}

	public void seek(int posn) {
		reproductor.seekTo(posn);
	}

	public void go() {
		reproductor.start();
	}

	// skip to previous track
	public void playPrev() {
		posicionCancion--;
		if (posicionCancion < 0)
			posicionCancion = canciones.size() - 1;
		reproducirCancion();
	}

	// skip to next
	public void playNext() {
		if (shuffle) {
			int newSong = posicionCancion;
			while (newSong == posicionCancion) {
				newSong = random.nextInt(canciones.size());
			}
			posicionCancion = newSong;
		} else {
			posicionCancion++;
			if (posicionCancion >= canciones.size())
				posicionCancion = 0;
		}
		reproducirCancion();
	}

	// binder
	public class MusicaBinder extends Binder {
		MusicaService getService() {
			return MusicaService.this;
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		if (reproductor.getCurrentPosition() > 0) {
			mp.reset();
			playNext();
		}

	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		mp.reset();
		return false;
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		// start playback
		mp.start();
		// notification
		Intent notIntent = new Intent(this, MainActivity.class);
		notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pendInt = PendingIntent.getActivity(this, 0, notIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

		builder.setContentIntent(pendInt).setSmallIcon(R.mipmap.ic_launcher)
				.setTicker(tituloCancion).setOngoing(true)
				.setContentTitle("Playing").setContentText(tituloCancion);
		Notification not = builder.build();
		startForeground(NOTIFY_ID, not);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return musicaBind;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		reproductor.stop();
		reproductor.release();
		return super.onUnbind(intent);
	}
	
	@Override
	public void onDestroy() {
		stopForeground(true);
	}

	

}
