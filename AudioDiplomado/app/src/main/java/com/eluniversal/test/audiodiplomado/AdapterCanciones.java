package com.eluniversal.test.audiodiplomado;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterCanciones extends ArrayAdapter<Cancion> {

	private Context context=null;
	private List<Cancion> canciones=null;
	public AdapterCanciones(Context context,
			List<Cancion> canciones) {
		super(context, R.layout.item_cancion, canciones);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.canciones=canciones;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
		          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View item = inflater.inflate(R.layout.item_cancion, parent, false);
		
		TextView tituloTextView = (TextView) item.findViewById(R.id.tituloTextView);
		TextView artistaTextView = (TextView) item.findViewById(R.id.artistaTextView);
		
		Cancion cancion=canciones.get(position);
		
		tituloTextView.setText(cancion.getTitulo());
		artistaTextView.setText(cancion.getArtista());
		
		return item;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return canciones.size();
	}
	
	@Override
	public Cancion getItem(int position) {
		// TODO Auto-generated method stub
		return canciones.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return canciones.get(position).getId();
	}
	
	

}
