package com.eluniversal.test.audiodiplomado;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;

public class ReproductorFragment extends Fragment implements MediaPlayerControl {

	public static final int WRITE_EXTERNAL_STORAGE_PERMISSION = 1266;
	public static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;

	private ListView cancionesListView = null;
	private ArrayList<Cancion> cancionesList = null;

	private MusicaService musicaService = null;
	private MusicaController musicaController = null;
	private Intent playIntent;
	private boolean reproductorPausado = false, pausado = false;
	private boolean musicBound = false;
	private AdapterCanciones adapter = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (checkPlayServices()) {
			cancionesListView = (ListView) getActivity().findViewById(
					R.id.cancionesListView);
			cancionesList = obtenerCanciones();

			if (cancionesList != null && cancionesList.size() != 0) {
				adapter = new AdapterCanciones(getActivity(), cancionesList);
				cancionesListView.setAdapter(adapter);

				setController();

				cancionesListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
																	int position, long id) {
						// TODO Auto-generated method stub
						musicaService.setCancion(position);
						musicaService.reproducirCancion();
						if (reproductorPausado) {
							setController();
							reproductorPausado = false;
						}
						musicaController.show();
					}
				});
			} else {
				Toast.makeText(getActivity(),
						"Se requiere Google Play Services",
						Toast.LENGTH_LONG).show();
			}


		}

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (playIntent == null) {
			playIntent = new Intent(getActivity(), MusicaService.class);
			getActivity().bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
			getActivity().startService(playIntent);
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (pausado) {
			setController();
			pausado = false;
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		musicaController.hide();
		super.onStop();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		getActivity().stopService(playIntent);
		musicaService = null;
		super.onDestroy();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		pausado = true;
	}

	//connect to the service
	private ServiceConnection musicConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MusicaService.MusicaBinder binder = (MusicaService.MusicaBinder) service;
			//get service
			musicaService = binder.getService();
			//pass list
			musicaService.setCanciones(cancionesList);
			musicBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			musicBound = false;
		}
	};


	public ArrayList<Cancion> obtenerCanciones() {
		ArrayList<Cancion> canciones = null;
		try {
			ContentResolver musicaResolver = getActivity().getContentResolver();
			Uri musicaUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
			Cursor musicaCursor = musicaResolver.query(musicaUri, null, null,
					null, null);
			if (musicaCursor != null && musicaCursor.moveToFirst()) {
				canciones = new ArrayList<Cancion>();
				int tituloColumna = musicaCursor
						.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
				int idColumna = musicaCursor
						.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
				int artistaColumna = musicaCursor
						.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST);
				// add songs to list
				do {
					long id = musicaCursor.getLong(idColumna);
					String titulo = musicaCursor.getString(tituloColumna);
					String artista = musicaCursor.getString(artistaColumna);
					canciones.add(new Cancion(id, titulo, artista));
				} while (musicaCursor.moveToNext());
			}
		} catch (Exception e) {
			Log.v("Error", e.getMessage());
		}
		return canciones;
	}

	private void setController() {
		musicaController = new MusicaController(getActivity());
		//set previous and next button listeners
		musicaController.setPrevNextListeners(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				playNext();
			}
		}, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				playPrev();
			}
		});
		//set and show
		musicaController.setMediaPlayer(this);
		musicaController.setAnchorView(getActivity().findViewById(R.id.cancionesListView));
		musicaController.setEnabled(true);
	}

	private void playNext() {
		musicaService.playNext();
		if (reproductorPausado) {
			setController();
			reproductorPausado = false;
		}
		musicaController.show(0);
	}

	private void playPrev() {
		musicaService.playPrev();
		if (reproductorPausado) {
			setController();
			reproductorPausado = false;
		}
		musicaController.show(0);
	}

	public boolean checkPlayServices() {
		int status = GoogleApiAvailability.getInstance()
				.isGooglePlayServicesAvailable(getContext());
		if (status != ConnectionResult.SUCCESS) {
			if (GoogleApiAvailability.getInstance().isUserResolvableError(status)) {
				mostrarErrorPlayServices(status);
			} else {
				Toast.makeText(getContext(),
						"Este dispositivo no soporta los servicios de Google Play",
						Toast.LENGTH_LONG).show();
			}
			return false;
		} else {
			return true;
		}
	}

	private void mostrarErrorPlayServices(int status) {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		apiAvailability.getErrorDialog(getActivity(),
				status, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
	}

	private boolean verificarPermisos() {
		boolean hasPermissionWrite = (ContextCompat.checkSelfPermission(getActivity(),
				Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

		if (!hasPermissionWrite) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
					Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
				DialogWarning dialogWarning = DialogWarning.newInstance("Aviso",
						"Se requieren permisos de lectura de tarjeta SD");
				dialogWarning.setOnDialogWarningListener(new OnDialogWarningListener() {
					@Override
					public void onAccept(Dialog dialog) {
						ActivityCompat.requestPermissions(getActivity(),
								new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
										Manifest.permission.ACCESS_FINE_LOCATION},
								WRITE_EXTERNAL_STORAGE_PERMISSION);
						dialog.dismiss();
					}

					@Override
					public void onCancel(Dialog dialog) {
						dialog.dismiss();
					}
				});
				dialogWarning.show(getFragmentManager(), "dialogo");
			} else {
				ActivityCompat.requestPermissions(getActivity(),
						new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSION);
			}
		}

		return hasPermissionWrite;
	}


	@Override
	public void start() {
		// TODO Auto-generated method stub
		musicaService.go();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		reproductorPausado = true;
		musicaService.pauseReproductor();
	}

	@Override
	public int getDuration() {
		// TODO Auto-generated method stub
		if (musicaService != null && musicBound && musicaService.isPng())
			return musicaService.getDur();
		else return 0;
	}

	@Override
	public int getCurrentPosition() {
		// TODO Auto-generated method stub
		if (musicaService != null && musicBound && musicaService.isPng())
			return musicaService.getPosicion();
		else return 0;
	}

	@Override
	public void seekTo(int pos) {
		// TODO Auto-generated method stub
		musicaService.seek(pos);
	}

	@Override
	public boolean isPlaying() {
		// TODO Auto-generated method stub
		if (musicaService != null && musicBound)
			return musicaService.isPng();
		return false;
	}

	@Override
	public int getBufferPercentage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean canPause() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canSeekForward() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}

}
