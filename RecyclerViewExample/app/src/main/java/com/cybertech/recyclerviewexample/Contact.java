package com.cybertech.recyclerviewexample;

import android.os.Build;

import java.util.Objects;

public class Contact {

  private int id=0;
  private String name=null;
  private String lastname=null;
  private String email=null;
  private int age=0;

  public Contact() {
  }

  public Contact(int id, String name, String lastname, String email, int age) {
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.email = email;
    this.age = age;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Contact contact = (Contact) o;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      return id == contact.id &&
          age == contact.age &&
          Objects.equals(name, contact.name) &&
          Objects.equals(lastname, contact.lastname) &&
          Objects.equals(email, contact.email);
    }else{
      if(id==contact.id && name.equals(contact.name)){
        return true;
      }else{
        return false;
      }
    }
  }

  @Override
  public int hashCode() {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      return Objects.hash(id, name, lastname, email, age);
    }else{
      return 5 * id + name.length();
    }
  }

  @Override
  public String toString() {
    return name + " " +lastname;
  }
}
