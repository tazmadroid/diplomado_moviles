package com.cybertech.recyclerviewexample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

  @NonNull
  @Override
  public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    return null;
  }

  @Override
  public void onBindViewHolder(@NonNull ContactViewHolder contactViewHolder, int i) {

  }

  @Override
  public int getItemCount() {
    return 0;
  }

  static class ContactViewHolder extends RecyclerView.ViewHolder{

    public ContactViewHolder(@NonNull View itemView) {
      super(itemView);
    }
  }
}
