package mx.unam.dgtic.curso.pruebaorientacion;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class MainFragment extends Fragment{
	
	private EditText nombreEditText=null;
	private EditText edadEditText=null;
	private EditText procedenciaEditText=null;
	
	public static MainFragment newInstance(){
		MainFragment mainFragment = new MainFragment();
		return mainFragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_main, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		nombreEditText = (EditText) getActivity().findViewById(R.id.nombreEditText);
		edadEditText = (EditText) getActivity().findViewById(R.id.edadEditText);
		procedenciaEditText = (EditText) getActivity().findViewById(R.id.procedenciaEditText);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
	    {
	        Toast.makeText(getActivity(), "portrait", Toast.LENGTH_LONG).show();
	    }
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
	    {
	        Toast.makeText(getActivity(), "landscape", Toast.LENGTH_LONG).show();
	    } 
	}

}
