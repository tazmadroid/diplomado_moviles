package com.cybertech.procesosdiplomado;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cybertech.procesosdiplomado.webservices.Entity;
import com.cybertech.procesosdiplomado.webservices.JSONParser;
import com.cybertech.procesosdiplomado.webservices.Petition;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

	private RecyclerView moviesRecyclerView=null;
	private List<Video> videos=null;
	private ProgressBar moviesProgressBar=null;
	private JSONParser jsonParser=null;

	public MainActivityFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		moviesProgressBar = (ProgressBar) view.findViewById(R.id.moviesProgressBar);
		moviesRecyclerView = (RecyclerView) view.findViewById(R.id.moviesRecyclerView);
		obtenerInformación();
	}

	private void obtenerInformación(){
		ObtenerInformacionAsyncTask obtenerInformacionAsyncTask = new ObtenerInformacionAsyncTask();
		obtenerInformacionAsyncTask.execute();
	}

	private void setLista(){
		LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(),
				LinearLayoutManager.VERTICAL,false);
		moviesRecyclerView.setHasFixedSize(true);
		moviesRecyclerView.setLayoutManager(layoutManager);
		VideoAdapter videoAdapter = new VideoAdapter(videos,getActivity());
		moviesRecyclerView.setAdapter(videoAdapter);
		moviesProgressBar.setVisibility(View.INVISIBLE);
	}

	private class ObtenerInformacionAsyncTask extends AsyncTask<Void,Integer,Boolean>{

		@Override
		protected Boolean doInBackground(Void... voids) {
			try {
				jsonParser = new JSONParser();
				videos = jsonParser.getJSON("http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json",
						new Petition(Entity.NONE));
				if(videos!=null && !videos.isEmpty()){
					return true;
				}else{
					return false;
				}
			}catch (Exception e){
				return false;
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			moviesProgressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(Boolean aBoolean) {
			super.onPostExecute(aBoolean);
			moviesProgressBar.setVisibility(View.GONE);
			if(aBoolean)
				setLista();
			else
				Toast.makeText(getActivity(), "Hubo un error", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			moviesProgressBar.setVisibility(View.GONE);
			Toast.makeText(getActivity(), "Hubo un error", Toast.LENGTH_SHORT).show();
		}
	}
}
