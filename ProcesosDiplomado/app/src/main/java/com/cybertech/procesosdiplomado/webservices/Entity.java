package com.cybertech.procesosdiplomado.webservices;

public enum Entity
{
    POST,
    GET,
    FRIENDLY,
    NONE
}
