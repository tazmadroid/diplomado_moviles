package com.cybertech.procesosdiplomado.webservices;

import android.util.Xml;

import com.cybertech.procesosdiplomado.Video;

import org.xmlpull.v1.XmlPullParser;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tazmadroid on 15/02/18.
 */

public class RSSParser {

  private String uri=null;
  private Petition petition=null;

  private HttpURLConnection httpURLConnection=null;

  public RSSParser() {
  }

  public RSSParser(String uri, Petition petition) {
    this.uri = uri;
    this.petition = petition;
  }

  public List<Video> obtenerVideos() throws Exception{
    List<Video> videos=null;
    XmlPullParser parser = Xml.newPullParser();
    try{
      parser.setInput(this.obtenerXML(), "UTF-8");
      int videoCursor=parser.getEventType();
      Video video=null;
      while(videoCursor!=XmlPullParser.END_DOCUMENT){
        String etiqueta=null;
        switch(videoCursor){
          case XmlPullParser.START_DOCUMENT:
            videos = new ArrayList<Video>();
            break;
          case XmlPullParser.START_TAG:
            etiqueta=parser.getName();
            if(etiqueta.equals("result")){
              video=new Video();
              video.setTitle(parser.getAttributeValue(0));
              video.setYear(parser.getAttributeValue(1));
              video.setId(parser.getAttributeValue(2));
              video.setType(parser.getAttributeValue(3));
              video.setPoster(parser.getAttributeValue(4));
              videos.add(video);
            }
            break;
          case XmlPullParser.END_TAG:
            etiqueta = parser.getName();
            break;
        }
        videoCursor=parser.next();
      }
    }catch(Exception e){
      throw new Exception();
    }
    return (videos);
  }

  public HttpURLConnection createURLConnection(String petitionUri, Petition petition) throws Exception {
    HttpURLConnection httpURLConnection = null;
    DataOutputStream wr = null;

    try {
      switch (petition.getEntity()) {
        case POST:
          URL urlpost = new URL(petitionUri);
          httpURLConnection = (HttpURLConnection) urlpost.openConnection();
          httpURLConnection.setInstanceFollowRedirects(false);
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("POST");
          httpURLConnection.setRequestProperty("Content-Type", "application/json");
          httpURLConnection.setRequestProperty("Accept", "application/json");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setRequestProperty("Accept-Encoding", "");
          httpURLConnection.setDoInput(true);
          httpURLConnection.setDoOutput(true);
          httpURLConnection.setUseCaches(false);
          wr = new DataOutputStream(httpURLConnection.getOutputStream());
          String jsonString = petition.getParamsPost();
          wr.write(jsonString.getBytes("UTF-8"));
          wr.flush();
          wr.close();
          break;
        case GET:
          String uri = petitionUri;
          if (petition.getParamsGet() != null)
            uri += petition.getParamsGet();
          URL urlGet = new URL(uri);
          httpURLConnection = (HttpURLConnection) urlGet.openConnection();
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("GET");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setUseCaches(false);
          break;
        case FRIENDLY:
          String uriFriendly = petitionUri;
          if (petition.getParamFriendly() != null)
            uriFriendly += petition.getParamFriendly();
          URL urlFriendly = new URL(uriFriendly);
          httpURLConnection = (HttpURLConnection) urlFriendly.openConnection();
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("GET");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setUseCaches(false);
          break;
        default:
          URL url = new URL(petitionUri);
          httpURLConnection = (HttpURLConnection) url.openConnection();
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("GET");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setUseCaches(false);
          break;
      }

    } catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    } catch (SocketTimeoutException f) {
      f.printStackTrace();
      return null;
    } catch (Exception g) {
      g.printStackTrace();
      return null;
    }


    return httpURLConnection;
  }

  private InputStream obtenerXML(){
    try{
      httpURLConnection = createURLConnection(uri,petition);
      return httpURLConnection.getInputStream();
    }catch(Exception e){
      return null;
    }
  }

}
