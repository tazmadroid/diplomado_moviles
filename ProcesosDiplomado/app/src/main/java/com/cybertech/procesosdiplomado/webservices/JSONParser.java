package com.cybertech.procesosdiplomado.webservices;

import android.os.Build;

import com.cybertech.procesosdiplomado.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tazmadroid on 14/02/18.
 */

public class JSONParser {
  HttpURLConnection connection = null;

  public JSONParser(){

  }

  public List<Video> getJSON(String uri, Petition petition){
    List<Video> videos =new ArrayList<>();
    try {
      JSONObject jsonObject = new JSONObject(getJSONString(uri,petition));
      if(jsonObject!=null){
          JSONArray videosJsonArray = jsonObject.getJSONArray("Search");
          if(videosJsonArray!=null){
            for (int indice=0;indice<videosJsonArray.length();indice++){
              JSONObject videoJSoJsonObject= videosJsonArray.getJSONObject(indice);
              videos.add(new Video(videoJSoJsonObject.getString("imdbID"),
                  videoJSoJsonObject.getString("Title"),
                  videoJSoJsonObject.getString("Year"),
                  videoJSoJsonObject.getString("Type"),
                  videoJSoJsonObject.getString("Poster")));
            }
          }
      }

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return videos;
  }

  private String getJSONString(String petitionUri, Petition petition) {
    InputStream inputStream = null;
    String jsonResult = null;
    try {
      connection = createURLConnection(petitionUri, petition);
      if (petition.getEntity() != Entity.POST) {
        inputStream = new BufferedInputStream(connection.getInputStream());
        jsonResult = convertInputStreamToString(inputStream);
      } else {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
          inputStream = new BufferedInputStream(connection.getInputStream());
          jsonResult = convertInputStreamToStringPost(inputStream);
        } else {
          inputStream = new BufferedInputStream(connection.getInputStream());
          jsonResult = convertInputStreamToString(inputStream);
        }
      }
    } catch (Exception e) {
      return null;
    }
    return jsonResult;
  }

  public HttpURLConnection createURLConnection(String petitionUri, Petition petition) throws Exception {
    HttpURLConnection httpURLConnection = null;
    DataOutputStream wr = null;

    try {
      switch (petition.getEntity()) {
        case POST:
          URL urlpost = new URL(petitionUri);
          httpURLConnection = (HttpURLConnection) urlpost.openConnection();
          httpURLConnection.setInstanceFollowRedirects(false);
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("POST");
          httpURLConnection.setRequestProperty("Content-Type", "application/json");
          httpURLConnection.setRequestProperty("Accept", "application/json");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setRequestProperty("Accept-Encoding", "");
          httpURLConnection.setDoInput(true);
          httpURLConnection.setDoOutput(true);
          httpURLConnection.setUseCaches(false);
          wr = new DataOutputStream(httpURLConnection.getOutputStream());
          String jsonString = petition.getParamsPost();
          wr.write(jsonString.getBytes("UTF-8"));
          wr.flush();
          wr.close();
          break;
        case GET:
          String uri = petitionUri;
          if (petition.getParamsGet() != null)
            uri += petition.getParamsGet();
          URL urlGet = new URL(uri);
          httpURLConnection = (HttpURLConnection) urlGet.openConnection();
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("GET");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setUseCaches(false);
          break;
        case FRIENDLY:
          String uriFriendly = petitionUri;
          if (petition.getParamFriendly() != null)
            uriFriendly += petition.getParamFriendly();
          URL urlFriendly = new URL(uriFriendly);
          httpURLConnection = (HttpURLConnection) urlFriendly.openConnection();
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("GET");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setUseCaches(false);
          break;
        default:
          URL url = new URL(petitionUri);
          httpURLConnection = (HttpURLConnection) url.openConnection();
          httpURLConnection.setConnectTimeout(petition.getTimeConnection());
          httpURLConnection.setReadTimeout(petition.getTimeConnection());
          httpURLConnection.setRequestMethod("GET");
          httpURLConnection.setRequestProperty("charset", "utf-8");
          httpURLConnection.setUseCaches(false);
          break;
      }

    } catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    } catch (SocketTimeoutException f) {
      f.printStackTrace();
      return null;
    } catch (Exception g) {
      g.printStackTrace();
      return null;
    }


    return httpURLConnection;
  }

  public static String convertInputStreamToString(InputStream inputStream) {
    String line = new String();
    String result = new String();
    try {
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

      while ((line = bufferedReader.readLine()) != null)
        result += line;

      inputStream.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;

  }

  public static String convertInputStreamToStringPost(InputStream inputStream) {
    String line = null;
    StringBuffer output = new StringBuffer("");
    try {
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

      line = "";
      while ((line = bufferedReader.readLine()) != null)
        output.append(line);

      inputStream.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    return output.toString();

  }
}
