package com.cybertech.procesosexample.webservices;

public enum Entity
{
  POST,
  GET,
  FRIENDLY,
  NONE
}
