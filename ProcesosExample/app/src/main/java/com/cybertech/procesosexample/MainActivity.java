package com.cybertech.procesosexample;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cybertech.procesosexample.webservices.Entity;
import com.cybertech.procesosexample.webservices.JSONParser;
import com.cybertech.procesosexample.webservices.Petition;

import java.util.List;

public class MainActivity extends AppCompatActivity {
  private ProgressBar progressBar=null;
  private RecyclerView moviesRecyclerView=null;
  private VideoAdapter videoAdapter=null;
  private JSONParser jsonParser=null;
  private List<Video> videos=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.
        Builder().permitAll().build();

    StrictMode.setThreadPolicy(policy);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    progressBar = findViewById(R.id.progressBar);
    moviesRecyclerView=findViewById(R.id.movies_recyclerview);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();
      }
    });

    obtenerInformacion();

/*

    new Thread(new Runnable() {
      @Override
      public void run() {
        try{
          jsonParser = new JSONParser();
          videos=jsonParser.getJSON("http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json",
              new Petition(Entity.NONE));
          runOnUiThread(mostrarListaVideos);
        }catch (Exception e){
          Looper.prepare();
          Message msj = mensajeError.obtainMessage();
          msj.obj=e.getMessage();
          mensajeError.sendMessage(msj);
          Looper.loop();
          Looper.myLooper().quit();
        }
      }
    }).start();*/
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private Runnable mostrarListaVideos=new Runnable() {
    @Override
    public void run() {
      setLista();
      progressBar.setVisibility(View.GONE);
    }
  };

  private Handler mensajeError = new Handler(){
    @Override
    public void handleMessage(Message msg) {
      super.handleMessage(msg);
      if(msg.obj!=null){
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getBaseContext(),"Ocurrio un error",Toast.LENGTH_LONG).show();
      }
    }
  };

  private void setLista(){
    LinearLayoutManager layoutManager=new LinearLayoutManager(getBaseContext(),
        LinearLayoutManager.VERTICAL,false);
    moviesRecyclerView.setHasFixedSize(true);
    moviesRecyclerView.setLayoutManager(layoutManager);
    videoAdapter=new VideoAdapter(videos,getBaseContext());
    moviesRecyclerView.setAdapter(videoAdapter);
  }

  private void obtenerInformacion(){
    ObtenerInformacionTask obtenerInformacionTask = new ObtenerInformacionTask();
    obtenerInformacionTask.execute();
  }

  private class ObtenerInformacionTask extends AsyncTask<Void,Integer,Boolean>{

    @Override
    protected Boolean doInBackground(Void... voids) {
      try{
        jsonParser = new JSONParser();
        videos=jsonParser.getJSON("http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json",
            new Petition(Entity.NONE));
        if(videos!=null){
          return true;
        }else{
          return false;
        }
      }catch (Exception e){
        e.printStackTrace();
        return false;
      }
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
      super.onPostExecute(aBoolean);
      progressBar.setVisibility(View.GONE);
      if(aBoolean){
        setLista();
      }else{
        Toast.makeText(getBaseContext(),"Ocurrio un error",Toast
            .LENGTH_LONG).show();
      }
    }
  }
}
