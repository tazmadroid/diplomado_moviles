package com.cybertech.procesosexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cybertech.procesosexample.webservices.Entity;
import com.cybertech.procesosexample.webservices.JSONParser;
import com.cybertech.procesosexample.webservices.Petition;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

  private List<Video> videos=null;
  private Context context=null;

  public VideoAdapter(List<Video> videos){
    this.videos=videos;
  }

  public VideoAdapter(List<Video> videos, Context context) {
    this.videos = videos;
    this.context = context;
  }

  @Override
  public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.
        item_video, parent,false);
    return new VideoViewHolder(item);
  }

  @Override
  public void onBindViewHolder(VideoViewHolder holder, int position) {
    holder.titleTextView.setText(videos.get(position).getTitle());
    holder.typeTextView.setText(videos.get(position).getType());
    BitmapFactory.Options options = new BitmapFactory.Options();
    //options.inJustDecodeBounds=true;
    options.inSampleSize=2;
    holder.thumbnailImageView.setImageBitmap(LoadImage(videos
        .get(position).getPoster(),options));

    //Glide.with(context).load(videos.get(position).getPoster()).into(holder.thumbnailImageView);
  }

  @Override
  public int getItemCount() {
    return videos.size();
  }

  private Bitmap LoadImage(String URL, BitmapFactory.Options options)
  {
    Bitmap bitmap = null;
    InputStream in = null;
    try {
      JSONParser rss = new JSONParser();
      in = rss.createURLConnection(URL,new Petition(Entity.NONE)).getInputStream();
      bitmap = BitmapFactory.decodeStream(in, null, options);
      in.close();
    } catch (IOException e1) {
      e1.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bitmap;
  }

  static class VideoViewHolder extends RecyclerView.ViewHolder{

    protected TextView titleTextView=null;
    protected TextView typeTextView=null;
    protected ImageView thumbnailImageView=null;

    public VideoViewHolder(View itemView) {
      super(itemView);
      titleTextView=(TextView) itemView.findViewById(R.id.titleTextView);
      typeTextView=(TextView) itemView.findViewById(R.id.typeTextView);
      thumbnailImageView=(ImageView) itemView.findViewById(R.id.thumbnailImageView);
    }
  }
}
