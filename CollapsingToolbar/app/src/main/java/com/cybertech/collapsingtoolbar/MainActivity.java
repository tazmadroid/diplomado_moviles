package com.cybertech.collapsingtoolbar;

import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;


public class MainActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener{

  private AppBarLayout appBarLayout=null;
  private CollapsingToolbarLayout collapsingToolbarLayout=null;
  private int scrollRange=-1;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    appBarLayout = findViewById(R.id.app_bar);
    collapsingToolbarLayout=findViewById(R.id.toolbar_layout);
    appBarLayout.addOnOffsetChangedListener(this);

  }

  @Override
  public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
    if(scrollRange==-1){
      scrollRange=appBarLayout.getTotalScrollRange();
    }

    if(scrollRange+i==0){
      collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(android.R.color.black));
      collapsingToolbarLayout.setContentScrimColor(getResources().getColor(android.R.color.black));
      if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
    }else{
      collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.colorPrimaryDark));
      collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.colorPrimaryDark));
      if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      getWindow().setStatusBarColor(getResources().getColor(android.R.color.transparent));
    }
  }
}
