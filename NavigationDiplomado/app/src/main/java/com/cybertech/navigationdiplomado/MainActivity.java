package com.cybertech.navigationdiplomado;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements NavigationView
		.OnNavigationItemSelectedListener {

	private DrawerLayout mainDrawerLayout = null;
	private NavigationView menuNavigationView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mainDrawerLayout = (DrawerLayout) findViewById(R.id.mainDrawerLayout);
		ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(getParent(),
				mainDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		mainDrawerLayout.addDrawerListener(actionBarDrawerToggle);
		actionBarDrawerToggle.syncState();


		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});

		menuNavigationView = (NavigationView) findViewById(R.id.menuNavigationView);
		menuNavigationView.setNavigationItemSelectedListener(this);
	}

	@Override
	public void onBackPressed() {
		if (mainDrawerLayout.isDrawerOpen(GravityCompat.START))
			mainDrawerLayout.closeDrawers();
		else
			super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()){
			case R.id.nav_home:
				getSupportFragmentManager().popBackStack();
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return true;
			case R.id.nav_movies:
				switchFragment(R.id.containerMain,VideosFragment.newInstance("Videos","FragmentVideos"),"VIDEOS");
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return true;
			case R.id.nav_notifications:
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return true;
			case R.id.nav_photos:
				switchFragment(R.id.containerMain,PhotosFragment.newInstance("Photos","FragmentPhotos"),"PHOTOS");
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return true;
			case R.id.nav_privacy_policy:
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return  true;
			case R.id.nav_settings:
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return true;
			default:
				mainDrawerLayout.closeDrawer(GravityCompat.START);
				return true;
		}

	}

	public void switchFragment(int idContainer, Fragment fragment, String tag) {
		FragmentManager fragmentManager = getSupportFragmentManager();

		if (fragment != null) {
			FragmentTransaction transaction = null;

			while (fragmentManager.popBackStackImmediate()) ;

			transaction = fragmentManager
					.beginTransaction()
					.replace(idContainer, fragment, tag);

			if (!(fragment instanceof HomeFragment)) {
				transaction.addToBackStack(tag);
			}
			transaction.commit();
		}
	}
}
