package com.cybertechdroid.android.ejerciciofacebookapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Internet {

	public static boolean verificarConexion(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(cm!=null){
			NetworkInfo netInfo[] = cm.getAllNetworkInfo();
			if(netInfo!=null){
				for(NetworkInfo info : netInfo){
					if(info.getState() == NetworkInfo.State.CONNECTED){
						return true;
					}
				}
			}
		}
		return false;
	}
}
