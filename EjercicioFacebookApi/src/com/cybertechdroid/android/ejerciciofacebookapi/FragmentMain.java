package com.cybertechdroid.android.ejerciciofacebookapi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

public class FragmentMain extends Fragment {

	private EditText mensajeEditText = null;
	private Button publicarButton = null;
	private Button publicarFacebookButton = null;
	private UiLifecycleHelper uiHelper = null;
	private static final String TAG = "MainFragment";

	@Override
	public View onCreateView(LayoutInflater inflater,
			  ViewGroup container,   Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		LoginButton authButton = (LoginButton) view
				.findViewById(R.id.authButton);
		authButton.setFragment(this);
		return view;
	}

	@Override
	public void onActivityCreated(  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		mensajeEditText = (EditText) getActivity().findViewById(R.id.editText1);
		publicarButton = (Button) getActivity().findViewById(R.id.button1);
		publicarFacebookButton = (Button) getActivity().findViewById(
				R.id.button2);
		if(Internet.verificarConexion(getActivity())){
			publicarButton.setEnabled(true);
			publicarFacebookButton.setEnabled(true);
		}else{
			publicarButton.setEnabled(false);
			publicarFacebookButton.setEnabled(false);
		}

		publicarButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mensaje = mensajeEditText.getText().toString();
				publicarDialogo(mensaje);
			}
		});

		publicarFacebookButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mensaje = mensajeEditText.getText().toString();
				publicarFacebook(mensaje);
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Usuario logueado");
		} else {
			Log.i(TAG, "Usuario no logueado");
		}
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			// TODO Auto-generated method stub
			onSessionStateChange(session, state, exception);
		}
	};

	private void publicarFacebook(String msj) {
		FacebookDialog dialogoFB = new FacebookDialog.ShareDialogBuilder(
				getActivity()).setName("Compartir").setDescription(msj)
				.setLink("www.google.com").build();
		uiHelper.trackPendingDialogCall(dialogoFB.present());
	}

	private void publicarDialogo(String msj) {
		Bundle params = new Bundle();
		params.putString("name", "Diplomado de Moviles");
		params.putString("caption", msj);
		params.putString("description", "Ejercicio de compartir en Facebook");
		params.putString("link", "http://www.google.com");

		WebDialog webDialog = (new WebDialog.FeedDialogBuilder(getActivity(),
				Session.getActiveSession(), params)).setOnCompleteListener(
				new OnCompleteListener() {

					@Override
					public void onComplete(Bundle values,
							FacebookException error) {
						// TODO Auto-generated method stub
						if (error == null) {
							final String postId = values.getString("post_id");
							if (postId != null) {
								Toast.makeText(getActivity(),
										"publicacion compartida",
										Toast.LENGTH_LONG).show();
							} else {
								Toast.makeText(getActivity(),
										"publicacion no compartida",
										Toast.LENGTH_LONG).show();
							}
						} else if (error instanceof FacebookOperationCanceledException) {
							Toast.makeText(getActivity(),
									"publicacion no compartida",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(getActivity(),
									"publicacion no compartida",
									Toast.LENGTH_LONG).show();
						}
					}
				}).build();
		webDialog.show();
	}
}
