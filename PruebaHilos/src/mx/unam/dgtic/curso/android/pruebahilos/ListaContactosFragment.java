package mx.unam.dgtic.curso.android.pruebahilos;

import java.util.List;

import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListaContactosFragment extends Fragment{

	private ListView contactosListView=null;
	private ProgressDialog progresoProgressDialog=null;
	private JSONParser parser=null;
	private List<Contacto> contactos = null;
	private ObtenerInformacionAsyncTask obtenerInformacion = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_main, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	    contactosListView = (ListView) getActivity().findViewById(R.id.contactosListView);
	    obtenerInformacion();
		
	}
	
	private void obtenerInformacion(){
		progresoProgressDialog = new ProgressDialog(getActivity());
		progresoProgressDialog.setTitle("cargando");
		progresoProgressDialog.setMessage("Obteniendo Información...");
		progresoProgressDialog.setCancelable(false);
	      
		obtenerInformacion = new ObtenerInformacionAsyncTask();
		obtenerInformacion.execute();
	}
	
	private class ObtenerInformacionAsyncTask extends AsyncTask<Void, Integer, Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try{
				parser = new JSONParser("http://diplomado.cybertechdroid.com/Webservice/");
				contactos=parser.obtenerContactos();
				if(contactos!=null && contactos.size()!=0){
					return true;
				}else{
					return false;
				}	
			}catch(JSONException e){
				return false;
			}catch (Exception f) {
				// TODO: handle exception
				return false;
			}
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progresoProgressDialog.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					ObtenerInformacionAsyncTask.this.cancel(true);
				}
			});
			progresoProgressDialog.show();
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result){
				ArrayAdapter<Contacto> contactosAdapter = new ArrayAdapter<Contacto>(getActivity(),android.R.layout.simple_spinner_item, contactos);
				contactosListView.setAdapter(contactosAdapter);
				progresoProgressDialog.dismiss();
			}else{
				Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
			}
			
		}
		
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			progresoProgressDialog.cancel();
		}
		
	}
}
