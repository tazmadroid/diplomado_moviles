package mx.unam.dgtic.curso.android.pruebahilos;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

	private String url=null;
	
	public JSONParser(String url){
		this.url=url;
	}
	
	private String getJSON() throws Exception{
		String datosJSON=null;
		try{
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(params, 3);
			HttpConnectionParams.setSoTimeout(params, 3);
			HttpClient cliente = new DefaultHttpClient();
			HttpResponse respuesta = cliente.execute(new HttpGet(this.url));
			HttpEntity datosEntity = respuesta.getEntity();
			if(datosEntity!=null){
				datosJSON = EntityUtils.toString(datosEntity);
			}
		}catch(Exception e){
			throw new Exception("Error al obtener el servicio");
		}
		return datosJSON;
	}
	
	public List<Contacto> obtenerContactos() throws JSONException, Exception{
		List<Contacto> contactos=null;
		try{
			JSONArray contactosJsonObject = new JSONArray(getJSON());
			if(contactosJsonObject.length()!=0){
				contactos=new ArrayList<Contacto>();
				for(int i=0;i<contactosJsonObject.length();i++){
					JSONObject contactoJSONObject = contactosJsonObject.getJSONObject(i);
					if(contactoJSONObject.length()!=0){
						JSONObject contactoJSON = contactoJSONObject.getJSONObject("Contacto");
						Contacto contacto = new Contacto();
						contacto.setId(contactoJSON.getInt("cont_id"));
						contacto.setNombre(contactoJSON.getString("cont_nombre"));
						contacto.setApellidos(contactoJSON.getString("cont_apellidos"));
						contacto.setDirecion(contactoJSON.getString("cont_direccion"));
						contacto.setColonia(contactoJSON.getString("cont_colonia"));
						contacto.setCodigo(contactoJSON.getString("cont_codigo"));
						contacto.setMunicipio(contactoJSON.getString("cont_municipio"));
						contacto.setDelegacion(contactoJSON.getString("cont_delegacion"));
						contacto.setEstado(contactoJSON.getString("cont_estado"));
						contacto.setCorreo(contactoJSON.getString("cont_correo"));
						contacto.setCelular(contactoJSON.getString("cont_celular"));
						contacto.setProcedencia(contactoJSON.getString("cont_procedencia"));
						contactos.add(contacto);
					}else{
						throw new Exception("No se pudo leer la información del contacto");
					}
				}
			}else{
				throw new Exception("No se pudo leer la información de los contactos");
			}
		}catch(JSONException e){
			throw new JSONException(e.getMessage());
		}catch (Exception f) {
			// TODO: handle exception
			throw new Exception(f.getMessage());
		}
		return contactos;
	}
}
