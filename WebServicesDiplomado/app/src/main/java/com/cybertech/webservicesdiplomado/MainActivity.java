package com.cybertech.webservicesdiplomado;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.cybertech.webservicesdiplomado.webservices.Entity;
import com.cybertech.webservicesdiplomado.webservices.JSONParser;
import com.cybertech.webservicesdiplomado.webservices.Petition;
import com.cybertech.webservicesdiplomado.webservices.RSSParser;

import java.util.List;

public class MainActivity extends AppCompatActivity {

  private RecyclerView countriesRecyclerView=null;
  private List<Video> videos=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

    StrictMode.setThreadPolicy(policy);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();
      }
    });

    JSONParser jsonParser = new JSONParser();

    RSSParser rssParser = new RSSParser("http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=xml",
        new Petition(Entity.NONE));
    try {
      videos = jsonParser.getJSON("http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json",
          new Petition(Entity.NONE));
      //videos= rssParser.obtenerVideos();
      countriesRecyclerView = (RecyclerView) findViewById(R.id.countriesRecyclerView);
      LinearLayoutManager layoutManager= new LinearLayoutManager(this,
          LinearLayoutManager.VERTICAL,false);
      countriesRecyclerView.setHasFixedSize(true);
      countriesRecyclerView.setLayoutManager(layoutManager);
      VideoAdapter videoAdapter = new VideoAdapter(videos);
      countriesRecyclerView.setAdapter(videoAdapter);
    } catch (Exception e) {
      e.printStackTrace();
    }
    Log.d("Prueba","ls");
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
