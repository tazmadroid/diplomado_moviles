package com.cybertech.webservicesdiplomado.webservices;

public enum Entity
{
    POST,
    GET,
    FRIENDLY,
    NONE
}
