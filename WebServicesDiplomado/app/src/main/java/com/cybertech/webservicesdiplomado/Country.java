package com.cybertech.webservicesdiplomado;

/**
 * Created by jaivetorrespineda on 15/02/18.
 */

public class Country {

	private String name=null;
	private String codeShort=null;
	private String codeLong=null;

	public Country() {

	}

	public Country(String name, String codeShort, String codeLong) {
		this.name = name;
		this.codeShort = codeShort;
		this.codeLong = codeLong;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCodeShort() {
		return codeShort;
	}

	public void setCodeShort(String codeShort) {
		this.codeShort = codeShort;
	}

	public String getCodeLong() {
		return codeLong;
	}

	public void setCodeLong(String codeLong) {
		this.codeLong = codeLong;
	}
}
