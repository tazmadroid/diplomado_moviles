package com.cybertech.fragmentsexample;


/**
 * Created by Tazmadroid on 27/01/18.
 */

public interface OnContactClickListener {

  public void onContactClick(Contact contact);
}
