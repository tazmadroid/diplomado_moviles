package com.cybertech.fragmentsexample;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

  public static final int REQUEST_UPDATE=0x46;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getSupportFragmentManager().beginTransaction().replace(R.id.mainContainer,
        MainFragment.newInstance(),"DETAIL-TAG").commit();
  }

  @Override
  protected void onActivityResult(int requestCode,int resultCode, Intent data){
    super.onActivityResult(requestCode,resultCode,data);
    Fragment fragment = getSupportFragmentManager().findFragmentByTag("DETAIL-TAG");
    if(fragment!=null)
      fragment.onActivityResult(requestCode,resultCode,data);
  }

  @Override
  public void onBackPressed() {
    boolean isTablet=getResources().getBoolean(R.bool.is_tablet);
    if(isTablet){
      Fragment fragment= getSupportFragmentManager().findFragmentByTag("DETAIL_TAG");
      getSupportFragmentManager().beginTransaction().hide(fragment).commit();
    }else {
      super.onBackPressed();
    }
  }
}
