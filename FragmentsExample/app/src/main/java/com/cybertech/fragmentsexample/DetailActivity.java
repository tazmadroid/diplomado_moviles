package com.cybertech.fragmentsexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class DetailActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    Toolbar toolbar = findViewById(R.id.toolbar_detail);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    
    Contact contact=(Contact)(getIntent().getExtras())
        .getSerializable(DetailFragment.KEY_CONTACT);
    getSupportFragmentManager().beginTransaction().replace(R.id.detailContainer,
        DetailFragment.newInstance(contact)).commit();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case android.R.id.home:
        Intent resultIntent=new Intent();
        setResult(MainActivity.REQUEST_UPDATE,resultIntent);
        finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
