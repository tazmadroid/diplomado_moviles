package com.cybertech.bottomnavigationview;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

  private BottomNavigationView mainBottomNavigationView=null;
  private ViewPager mainViewPager=null;
  private MusicAdapter musicAdapter=null;

  private MenuItem itemSelectedPrev=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    mainBottomNavigationView=findViewById(R.id.mainBottonNavigation);
    mainViewPager=findViewById(R.id.mainViewPager);

    musicAdapter = new MusicAdapter(getSupportFragmentManager());
    musicAdapter.addFragment(HomeFragment.newInstance("",""));
    musicAdapter.addFragment(LibraryFragment.newInstance("",""));
    musicAdapter.addFragment(CategoryFragment.newInstance("",""));
    musicAdapter.addFragment(RecordFragment.newInstance("",""));

    mainViewPager.setAdapter(musicAdapter);

    mainBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView
        .OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
          case R.id.home_item:
            mainViewPager.setCurrentItem(0);
            return true;
          case R.id.library_item:
            mainViewPager.setCurrentItem(1);
            return true;
          case R.id.record_item:
            mainViewPager.setCurrentItem(3);
            return true;
          case R.id.category_item:
            mainViewPager.setCurrentItem(2);
            return true;
          default:
            mainViewPager.setCurrentItem(0);
            return true;
        }
      }
    });

    mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int i, float v, int i1) {

      }

      @Override
      public void onPageSelected(int i) {
        if(itemSelectedPrev!=null)
          itemSelectedPrev.setChecked(false);
        else
          mainBottomNavigationView.getMenu().getItem(0).setChecked(false);

        mainBottomNavigationView.getMenu().getItem(i).setChecked(true);
        itemSelectedPrev=mainBottomNavigationView.getMenu().getItem(i);
      }

      @Override
      public void onPageScrollStateChanged(int i) {

      }
    });
  }
}
