package com.cybertech.bottomnavigationview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MusicAdapter extends FragmentPagerAdapter {

  private List<Fragment> fragments=null;

  public MusicAdapter(FragmentManager fm) {
    super(fm);
    fragments=new ArrayList<>();
  }

  public void addFragment(Fragment fragment){
    fragments.add(fragment);
  }

  @Override
  public Fragment getItem(int i) {
    return fragments.get(i);
  }

  @Override
  public int getCount() {
    return fragments.size();
  }
}
