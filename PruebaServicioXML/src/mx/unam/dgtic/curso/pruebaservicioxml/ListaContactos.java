package mx.unam.dgtic.curso.pruebaservicioxml;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListaContactos extends Fragment{
	
	private ListView contactosListView=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_main, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		try{
		RssParser parser = new RssParser("http://diplomado.cybertechdroid.com/Webservice/xml");
		List<Contacto> contactos=parser.obtenerContacto();
		contactosListView = (ListView) getActivity().findViewById(R.id.contactosListView);
		ArrayAdapter<Contacto> adapter = new ArrayAdapter<Contacto>(getActivity(), android.R.layout.simple_spinner_item, contactos);
		contactosListView.setAdapter(adapter);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

}
