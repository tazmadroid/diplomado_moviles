package mx.unam.dgtic.curso.pruebaservicioxml;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;

import android.util.Xml;

public class RssParser {

	private URL urlWebservice=null;
	
	public RssParser(){
		
	}
	
	public RssParser(String url) throws Exception{
		try{
			this.urlWebservice=new URL(url);
		}catch(Exception e){
			throw new Exception("Ocurrio un error al obtener el webservice");
		}
	}
	
	public List<Contacto> obtenerContacto() throws Exception{
		List<Contacto> contactos=null;
		XmlPullParser parser = Xml.newPullParser();
		try{
			parser.setInput(this.obtenerXML(), "UTF-8");
			int contactoCursor=parser.getEventType();
			Contacto contacto=null;
			while(contactoCursor!=XmlPullParser.END_DOCUMENT){
				String etiqueta=null;
				switch(contactoCursor){
				  case XmlPullParser.START_DOCUMENT:
					  contactos = new ArrayList<Contacto>();
					  break;
				  case XmlPullParser.START_TAG:
					  etiqueta=parser.getName();
					  if(etiqueta.equals("Contacto")){
						  contacto=new Contacto();
					  } else if(contacto!=null){
						  if(etiqueta.equals("cont_id")){
							  contacto.setId(Integer.parseInt(parser.nextText()));
						  }else if(etiqueta.equals("cont_nombre")){
							  contacto.setNombre(parser.nextText());
						  }else if(etiqueta.equals("cont_apellidos")){
							  contacto.setApellidos(parser.nextText());
						  }else if(etiqueta.equals("cont_direccion")){
							  contacto.setDirecion(parser.nextText());
						  }else if(etiqueta.equals("cont_colonia")){
							  contacto.setColonia(parser.nextText());
						  }else if(etiqueta.equals("cont_codigo")){
							  contacto.setCodigo(parser.nextText());
						  }else if(etiqueta.equals("cont_municipio")){
							  contacto.setMunicipio(parser.nextText());
						  }else if(etiqueta.equals("cont_delegacion")){
							  contacto.setDelegacion(parser.nextText());
						  }else if(etiqueta.equals("cont_estado")){
							  contacto.setEstado(parser.nextText());
						  }else if(etiqueta.equals("cont_correo")){
							  contacto.setCorreo(parser.nextText());
						  }else if(etiqueta.equals("cont_celular")){
							  contacto.setCelular(parser.nextText());
						  }else if(etiqueta.equals("cont_procedencia")){
							  contacto.setProcedencia(parser.nextText());
						  }
					  }
					  break;
				   case XmlPullParser.END_TAG:
					   etiqueta = parser.getName();
					   if(etiqueta.equals("Contacto") && contacto!=null){
						   contactos.add(contacto);
					   }
					   break;
				}
				contactoCursor=parser.next();
			}
		}catch(Exception e){
			throw new Exception();
		}
		return (contactos);
	}
	
	private InputStream obtenerXML(){
		try{
			return urlWebservice.openConnection().getInputStream();
		}catch(Exception e){
			return null;
		}
	}
}
