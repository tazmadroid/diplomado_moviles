package mx.unam.dgtic.android.pruebadrawer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PrincipalFragment extends Fragment{

	private ListView lista=null;
	private String[] contactos=null;
	private ArrayAdapter<String> adapterLista=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.principal_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		lista = (ListView) getActivity().findViewById(R.id.listaListView);
		
		contactos = new String[]{
			"Juan Solis","Jose Hernandez","Julian Mejia", "Carlos Soto",
			"Jorge Nava","Abraham Carrillo","Jesus Mejia"
		};
		
		adapterLista = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, contactos);
		lista.setAdapter(adapterLista);
	}
}
