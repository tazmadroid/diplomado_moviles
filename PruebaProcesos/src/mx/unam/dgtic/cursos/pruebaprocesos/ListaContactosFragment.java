package mx.unam.dgtic.cursos.pruebaprocesos;

import java.util.List;

import org.json.JSONException;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListaContactosFragment extends Fragment{
	
	private ListView contactosListView=null;
	private ProgressDialog progreso=null;
	private Thread progresoThread=null;
	private List<Contacto> contactos=null;
	private JSONParser parser=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.lista_contactos_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		contactosListView = (ListView) getActivity().findViewById(R.id.contactosListView);
		
		progreso = ProgressDialog.show(getActivity(), 
				"Obteniendo Información", "cargando...", true);
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
				parser=new JSONParser("http://diplomado.cybertechdroid.com/Webservice/");
				contactos = parser.obtenerContactos();
				getActivity().runOnUiThread(mostrarListaRunnable);
				}catch(JSONException e){
					Looper.prepare();
		            Message msj = mensajeError.obtainMessage();
		            msj.obj = e.getMessage();
		            mensajeError.sendMessage(msj);
		            Looper.loop();
		            Looper.myLooper().quit();
				}catch(Exception f){
					Looper.prepare();
		            Message msj = mensajeError.obtainMessage();
		            msj.obj = f.getMessage();
		            mensajeError.sendMessage(msj);
		            Looper.loop();
		            Looper.myLooper().quit();
				}
			}
		}).start();
		
		
	}
	
	private Runnable mostrarListaRunnable = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			ArrayAdapter<Contacto> adapterContactos = new ArrayAdapter<Contacto>(getActivity(), android.R.layout.simple_spinner_item, contactos);
			contactosListView.setAdapter(adapterContactos);	
			progreso.dismiss();
		}
	};
	
	private Handler mensajeError = new Handler() {
	    public void handleMessage(Message msg) {
	      if (msg.obj != null) {
	        progreso.dismiss();
	        Toast.makeText(getActivity(), (String) msg.obj, Toast.LENGTH_LONG)
	            .show();
	      }
	    };
	  };

}
