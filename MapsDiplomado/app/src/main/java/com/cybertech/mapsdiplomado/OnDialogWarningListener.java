package com.cybertech.mapsdiplomado;

import android.app.Dialog;

/**
 * Created by jaivetorrespineda on 09/03/18.
 */


public interface OnDialogWarningListener {

	public void onAccept(Dialog dialog);

	public void onCancel(Dialog dialog);
}

