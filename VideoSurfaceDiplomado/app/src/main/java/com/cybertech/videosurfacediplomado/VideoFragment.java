package com.cybertech.videosurfacediplomado;


import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;



public class VideoFragment extends Fragment implements SurfaceHolder.Callback,
    MediaPlayer.OnPreparedListener{

  private MediaPlayer mediaPlayer=null;
  private SurfaceHolder videoHolder=null;
  private SurfaceView videoSurfaceView=null;

  private String urlVideo="https://archive.org/download/ksnn_compilation_master" +
      "_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";

  public VideoFragment() {
    // Required empty public constructor
  }

  public static VideoFragment newInstance() {
    VideoFragment fragment = new VideoFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_video, container, false);
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    videoSurfaceView = (SurfaceView) getActivity()
        .findViewById(R.id.videoSurfaceView);
    videoHolder = videoSurfaceView.getHolder();
    videoHolder.addCallback(this);
  }

  @Override
  public void onPrepared(MediaPlayer mp) {
    mediaPlayer.start();
  }

  @Override
  public void surfaceCreated(SurfaceHolder holder) {
    try{
      mediaPlayer=new MediaPlayer();
      mediaPlayer.setDisplay(videoHolder);
      mediaPlayer.setDataSource(urlVideo);
      mediaPlayer.prepare();
      mediaPlayer.setOnPreparedListener(this);
      mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }catch (Exception e){
      Log.e("[EEROR]",e.getMessage());
    }
  }

  @Override
  public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

  }

  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {

  }
}
