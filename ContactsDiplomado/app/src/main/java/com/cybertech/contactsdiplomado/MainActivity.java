package com.cybertech.contactsdiplomado;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

  private ViewPager mainViewPager=null;
  private BottomNavigationView navigation=null;
  private MusicAdapter musicAdapter=null;

  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
      = new BottomNavigationView.OnNavigationItemSelectedListener() {

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
      switch (item.getItemId()) {
        case R.id.navigation_home:

          return true;
        case R.id.navigation_dashboard:

          return true;
        case R.id.navigation_notifications:

          return true;
      }
      return false;
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    navigation = (BottomNavigationView) findViewById(R.id.navigation);
    mainViewPager=(ViewPager) findViewById(R.id.mainViewPager);

    musicAdapter = new MusicAdapter(getSupportFragmentManager());
    musicAdapter.addFragment(ContactsFragment.newInstance());
    musicAdapter.addFragment(RegisterContactFragment.newInstance("",""));
    musicAdapter.addFragment(HistoryFragment.newInstance("",""));
    musicAdapter.addFragment(FavoritesFragment.newInstance("",""));

    mainViewPager.setAdapter(musicAdapter);

    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
  }

}
