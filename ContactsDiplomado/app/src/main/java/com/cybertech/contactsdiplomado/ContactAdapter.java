package com.cybertech.contactsdiplomado;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tazmadroid on 27/01/18.
 */

public class ContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  public static final int CONTACT=1;
  public static final int CONTACT_FAVORITE=2;

  private List<Contact> contacts=null;
  private OnContactClickListener onContactClickListener=null;

  public ContactAdapter(List<Contact> contacts) {
    if(contacts==null){
      this.contacts=new ArrayList<>();
    }else {
      this.contacts = contacts;
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if(viewType==CONTACT) {
      View item = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_contact, parent, false);
      ContactViewHolder contactViewHolder = new ContactViewHolder(item);
      contactViewHolder.setOnContactClickListener(onContactClickListener);
      return contactViewHolder;
    }else{
      View item = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_contact_favorite, parent, false);
      ContactViewFavoriteHolder contactViewFavoriteHolder = new ContactViewFavoriteHolder(item);
      contactViewFavoriteHolder.setOnContactClickListener(onContactClickListener);
      return contactViewFavoriteHolder;
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    if(getItemViewType(position)==CONTACT){
      ContactViewHolder contactViewHolder=(ContactViewHolder)holder;
      contactViewHolder.setContact(contacts.get(position));
      contactViewHolder.nameTextView.setText(contacts.get(position).getName());
      contactViewHolder.schoolTextView.setText(contacts.get(position).getSchool());
      contactViewHolder.ageTextView.setText(Integer.toString(contacts.get(position).getAge()));
    }else if(getItemViewType(position)==CONTACT_FAVORITE){
      ContactViewFavoriteHolder contactViewFavoriteHolder = (ContactViewFavoriteHolder) holder;
      contactViewFavoriteHolder.setContact(contacts.get(position));
      contactViewFavoriteHolder.nameTextView.setText(contacts.get(position).getName());
    }

  }

  @Override
  public int getItemCount() {
    return contacts.size();
  }

  @Override
  public int getItemViewType(int position) {
    if(this.contacts.isEmpty()){
      return CONTACT;
    }else{
      return this.contacts.get(position).getType();
    }
  }

  public void setOnContactClickListener(OnContactClickListener onContactClickListener) {
    this.onContactClickListener = onContactClickListener;
  }

  static class ContactViewHolder extends RecyclerView.ViewHolder{

    protected TextView nameTextView=null;
    protected TextView schoolTextView=null;
    protected TextView ageTextView=null;
    private Contact contact=null;

    private OnContactClickListener onContactClickListener=null;

    public ContactViewHolder(View itemView) {
      super(itemView);
      nameTextView=(TextView) itemView.findViewById(R.id.textView);
      schoolTextView=(TextView) itemView.findViewById(R.id.textView2);
      ageTextView=(TextView) itemView.findViewById(R.id.textView3);

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(onContactClickListener!=null && contact!=null)
            onContactClickListener.onContactClick(contact);
        }
      });
    }

    public ContactViewHolder(View itemView, Contact contact) {
      this(itemView);
      this.contact = contact;
    }

    public void setContact(Contact contact) {
      this.contact = contact;
    }

    public void setOnContactClickListener(OnContactClickListener onContactClickListener) {
      this.onContactClickListener = onContactClickListener;
    }
  }

  static class ContactViewFavoriteHolder extends RecyclerView.ViewHolder{

    protected TextView nameTextView=null;
    private Contact contact=null;

    private OnContactClickListener onContactClickListener=null;

    public ContactViewFavoriteHolder(View itemView) {
      super(itemView);
      nameTextView=(TextView) itemView.findViewById(R.id.nameTextView);

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(onContactClickListener!=null && contact!=null)
            onContactClickListener.onContactClick(contact);
        }
      });
    }

    public ContactViewFavoriteHolder(View itemView, Contact contact) {
      this(itemView);
      this.contact = contact;
    }

    public void setContact(Contact contact) {
      this.contact = contact;
    }

    public void setOnContactClickListener(OnContactClickListener onContactClickListener) {
      this.onContactClickListener = onContactClickListener;
    }
  }
}
