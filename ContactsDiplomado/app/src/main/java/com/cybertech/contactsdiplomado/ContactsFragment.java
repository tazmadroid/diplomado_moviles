package com.cybertech.contactsdiplomado;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ContactsFragment extends Fragment {

  private RecyclerView contactsRecyclerView=null;

  public ContactsFragment() {
    // Required empty public constructor
  }


  public static ContactsFragment newInstance() {
    ContactsFragment fragment = new ContactsFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_contacts, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    contactsRecyclerView =(RecyclerView) view.findViewById(R.id.contactsRecyclerView);
    LinearLayoutManager manager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
    contactsRecyclerView.setHasFixedSize(true);
    contactsRecyclerView.setLayoutManager(manager);
    ContactAdapter contactAdapter = new ContactAdapter(getContacts());
    contactsRecyclerView.setAdapter(contactAdapter);
    contactAdapter.setOnContactClickListener(new OnContactClickListener() {
      @Override
      public void onContactClick(Contact contact) {
        Toast.makeText(getContext(),contact.getName(),Toast.LENGTH_SHORT).show();
      }
    });
  }

  public List<Contact> getContacts(){
    List<Contact> contacts = new ArrayList<>();
    contacts.add(new Contact(1,"Juan Solis 1","Benemerito",ContactAdapter.CONTACT));
    contacts.add(new Contact(2,"Juan Solis 2","Benemerito",ContactAdapter.CONTACT_FAVORITE));
    contacts.add(new Contact(3,"Juan Solis 3","Benemerito",ContactAdapter.CONTACT));
    contacts.add(new Contact(4,"Juan Solis 4","Benemerito",ContactAdapter.CONTACT_FAVORITE));
    contacts.add(new Contact(5,"Juan Solis 5","Benemerito",ContactAdapter.CONTACT));
    contacts.add(new Contact(6,"Juan Solis 6","Benemerito",ContactAdapter.CONTACT_FAVORITE));
    contacts.add(new Contact(7,"Juan Solis 7","Benemerito",ContactAdapter.CONTACT));
    contacts.add(new Contact(8,"Juan Solis 8","Benemerito",ContactAdapter.CONTACT_FAVORITE));
    contacts.add(new Contact(9,"Juan Solis 9","Benemerito",ContactAdapter.CONTACT));
    contacts.add(new Contact(10,"Juan Solis 10","Benemerito",ContactAdapter.CONTACT_FAVORITE));
    contacts.add(new Contact(11,"Juan Solis 11","Benemerito",ContactAdapter.CONTACT));
    contacts.add(new Contact(12,"Juan Solis 12","Benemerito",ContactAdapter.CONTACT_FAVORITE));
    return contacts;
  }
}
