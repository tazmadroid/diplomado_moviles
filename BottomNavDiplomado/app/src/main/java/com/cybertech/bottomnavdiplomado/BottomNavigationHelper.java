package com.cybertech.bottomnavdiplomado;

import android.annotation.SuppressLint;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;

import java.lang.reflect.Field;

/**
 * Created by Tazmadroid on 10/02/18.
 */

public class BottomNavigationHelper {
  @SuppressLint("RestrictedApi")
  public static void removeShiftMode(BottomNavigationView bottomNavigationView){
    BottomNavigationMenuView menuView = (BottomNavigationMenuView)
        bottomNavigationView.getChildAt(0);
    try{
      Field shiftingMode =menuView.getClass()
          .getDeclaredField("mShiftingMode");
      shiftingMode.setAccessible(true);
      shiftingMode.setBoolean(menuView,false);
      shiftingMode.setAccessible(false);
      for (int indice=0;indice<menuView.getChildCount();indice++){
        BottomNavigationItemView itemView=(BottomNavigationItemView) menuView
            .getChildAt(indice);
        itemView.setShiftingMode(false);
        itemView.setChecked(itemView.getItemData().isChecked());
      }

    }catch (NoSuchFieldException e){

    }catch (IllegalAccessException f){

    }
  }
}
