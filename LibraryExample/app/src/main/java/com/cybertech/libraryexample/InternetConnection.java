package com.cybertech.libraryexample;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnection {

  public static NetworkInfo getNetworkInfo(Context context){
    ConnectivityManager cm = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    if(cm!=null)
      return cm.getActiveNetworkInfo();
    else
      return null;
  }

  public static boolean isConnectionAvailable(Context context){
    try{
      NetworkInfo networkInfo=getNetworkInfo(context);
      if(networkInfo!=null && networkInfo.isConnected()){
        return true;
      }else{
        return false;
      }
    }catch (Exception e){
      e.printStackTrace();
      return false;
    }
  }

  public static boolean isConnectionWifi(Context context){
    try{
      NetworkInfo networkInfo = getNetworkInfo(context);
      return  (networkInfo!=null &&networkInfo.isConnected() && networkInfo.getType()==ConnectivityManager.TYPE_WIFI);
    }catch (Exception e){
      e.printStackTrace();
      return false;
    }
  }
}
