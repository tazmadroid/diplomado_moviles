package com.cybertech.libraryexample;

import android.app.DownloadManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

  private RecyclerView videosRecyclerView=null;

  private VolleyService volleyService=null;
  private RequestQueue requestQueue=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    volleyService = VolleyService.getInstance(getApplicationContext());

    videosRecyclerView = findViewById(R.id.videos_recyclerview);
    LinearLayoutManager layoutManager = new LinearLayoutManager(this,
        LinearLayoutManager.VERTICAL,false);
    videosRecyclerView.setHasFixedSize(true);
    videosRecyclerView.setLayoutManager(layoutManager);

    if(InternetConnection.isConnectionAvailable(getBaseContext()))
      getVideos();
    else
      Toast.makeText(getBaseContext(),"No hay conexion",Toast.LENGTH_LONG).show();

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();
      }
    });


  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  public void getVideos(){
    String url = "http://www.omdbapi.com/?s=superman&apikey=2b28d307&r=json";
    JsonObjectRequest request = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
      @Override
      public void onResponse(JSONObject response) {
        Gson gson= new Gson();
        Search search  = gson.fromJson(response.toString(),Search.class);
        setLista(search.getSearch());
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        Toast.makeText(getBaseContext(),"Ocurrio un error",
            Toast.LENGTH_LONG).show();
      }
    });
    volleyService.addRequest(request);


  }

  public void setLista(List<Video> videos){
    VideoAdapter videoAdapter = new VideoAdapter(videos,getBaseContext());
    videosRecyclerView.setAdapter(videoAdapter);
  }
}
