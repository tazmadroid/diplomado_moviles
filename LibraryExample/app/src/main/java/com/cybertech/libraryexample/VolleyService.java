package com.cybertech.libraryexample;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyService {

  private static VolleyService volleyService=null;

  private RequestQueue requestQueue=null;

  private VolleyService(Context context){
    requestQueue=Volley.newRequestQueue(context);
  }

  public static VolleyService getInstance(Context context){
    if(volleyService==null)
      volleyService = new VolleyService(context);
    return volleyService;
  }

  public RequestQueue getRequestQueue(){
    return requestQueue;
  }

  public <T> void addRequest(Request<T> request){
    requestQueue.add(request);
  }
}
