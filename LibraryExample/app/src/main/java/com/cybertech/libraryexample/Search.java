package com.cybertech.libraryexample;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search {

  @SerializedName("Search")
  private List<Video> search=null;

  public Search() {
  }

  public Search(List<Video> search) {
    this.search = search;
  }

  public List<Video> getSearch() {
    return search;
  }

  public void setSearch(List<Video> search) {
    this.search = search;
  }
}
