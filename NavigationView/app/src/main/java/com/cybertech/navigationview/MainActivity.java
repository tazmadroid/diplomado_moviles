package com.cybertech.navigationview;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

  private DrawerLayout mainDrawerLayout=null;
  private NavigationView menuNavigationView=null;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    mainDrawerLayout = findViewById(R.id.main_drawerlayout);
    menuNavigationView = findViewById(R.id.menu_navigationview);
    ActionBarDrawerToggle actionBarDrawerToggle = new
        ActionBarDrawerToggle(getParent(),
        mainDrawerLayout,toolbar,R.string.navigation_drawer_open,R.string
        .navigation_drawer_close);
    mainDrawerLayout.addDrawerListener(actionBarDrawerToggle);
    actionBarDrawerToggle.syncState();

    menuNavigationView.setNavigationItemSelectedListener(this);

    /*
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.containerMain,HomeFragment.newInstance("","")).commit();*/


  }

  @Override
  public void onBackPressed() {
    if(mainDrawerLayout.isDrawerOpen(GravityCompat.START)){
      mainDrawerLayout.closeDrawer(GravityCompat.START);
    }else {
      super.onBackPressed();
    }

  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
    switch (menuItem.getItemId()){
      case R.id.nav_home:
        getSupportFragmentManager().popBackStack();
        mainDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
      case R.id.nav_movies:
        switchFragment(R.id.containerMain,VideosFragment.newInstance("Videos","FragmentVideos"),"TAG-VIDEOS");
        mainDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
      case R.id.nav_notifications:
        return true;
      case R.id.nav_photos:
        switchFragment(R.id.containerMain,PhotosFragment.newInstance("Fotos","FragmentFotos"),"TAG-FOTOS");
        mainDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
      case R.id.nav_privacy_policy:
        Intent privacyIntent=new Intent(getBaseContext(),PrivacyActivity.class);
        startActivity(privacyIntent);
        return true;
      case R.id.nav_settings:
        return true;
      case R.id.nav_about_us:
        return true;
      default:
        return true;
    }
  }

  public void switchFragment(int idContainer, Fragment fragment, String tag){
    FragmentManager fragmentManager = getSupportFragmentManager();
    if(fragment!=null){
      FragmentTransaction transaction=null;

      while (fragmentManager.popBackStackImmediate());

        transaction=fragmentManager.beginTransaction()
            .replace(idContainer,fragment,tag);

        if(!(fragment instanceof HomeFragment))
          transaction.addToBackStack(tag);

        transaction.commit();


    }
  }
}
